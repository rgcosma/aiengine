/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "PacketDispatcher.h"
#include <boost/exception/diagnostic_information.hpp> 
#include <boost/exception_ptr.hpp> 

namespace aiengine {

#ifdef HAVE_LIBLOG4CXX
log4cxx::LoggerPtr PacketDispatcher::logger(log4cxx::Logger::getLogger("aiengine.packetdispatcher"));
#endif

PacketDispatcher::PacketDispatcher(const std::string &source):
	status_(PacketDispatcherStatus::STOP), // Status of the PacketDispatcher
        stream_(),
	pcap_file_ready_(false),
	read_in_progress_(false),
        device_is_ready_(false),
	have_evidences_(false),
#if defined(STAND_ALONE_TEST) || defined(TESTING)
        max_packets_(std::numeric_limits<int32_t>::max()),
#endif
        total_packets_(0),total_bytes_(0),
	pcap_(nullptr),
        io_service_(),
        signals_(io_service_, SIGINT, SIGTERM),
        stats_(),
	header_(nullptr),
	pkt_data_(nullptr),
	idle_function_([&] (void) {}),
        eth_(nullptr),
	current_packet_(),
	defMux_(nullptr),
	stack_name_(),
	input_name_(source),
        pcap_filter_(),
        em_(SharedPointer<EvidenceManager>(new EvidenceManager())),
        current_network_stack_()
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
	,tm_(SharedPointer<TimerManager>(new TimerManager(io_service_))),
        user_shell_(SharedPointer<Interpreter>(new Interpreter(io_service_)))
#if defined(PYTHON_BINDING)
        ,pystack_()
#endif
#endif
	{
        signals_.async_wait(
        	boost::bind(&boost::asio::io_service::stop, &io_service_));
}


PacketDispatcher::~PacketDispatcher() { 

	io_service_.stop(); 
}

void PacketDispatcher::info_message(const std::string &msg) {

#ifdef HAVE_LIBLOG4CXX
        LOG4CXX_INFO(logger, msg);
#else
        std::chrono::system_clock::time_point time_point = std::chrono::system_clock::now();
        std::time_t now = std::chrono::system_clock::to_time_t(time_point);
#ifdef __clang__
        std::cout << "[" << std::put_time(std::localtime(&now), "%D %X") << "] ";
#else
        char mbstr[100];
        std::strftime(mbstr, 100, "%D %X", std::localtime(&now));
        std::cout << "[" << mbstr << "] ";
#endif
        std::cout << msg << std::endl;
#endif
}

void PacketDispatcher::statistics() {

        statistics(std::cout);
}

void PacketDispatcher::statistics(std::basic_ostream<char> &out) const {

        out << *this;
        if (have_evidences_) {
                em_->statistics(out);
        }
}

void PacketDispatcher::setStack(const SharedPointer<NetworkStack> &stack) {

	current_network_stack_ = stack;
	stack_name_ = stack->getName();
        setDefaultMultiplexer(stack->getLinkLayerMultiplexer().lock());
        stack->setAsioService(io_service_);
}

void PacketDispatcher::setDefaultMultiplexer(MultiplexerPtr mux) {

	defMux_ = mux;
	auto proto = mux->getProtocol();
	eth_ = std::dynamic_pointer_cast<EthernetProtocol>(proto);
}

int PacketDispatcher::get_mtu_of_network_device(const std::string &name) {

	struct ifreq ifr;
        int fd = socket(AF_INET, SOCK_DGRAM, 0);

	if (fd != -1) {
        	ifr.ifr_addr.sa_family = AF_INET;
        	strncpy(ifr.ifr_name , name.c_str() , IFNAMSIZ-1);

		if (ioctl(fd, SIOCGIFMTU, &ifr) == 0) {
			// Use the global namespace for link with the system call close
			::close(fd);
			return ifr.ifr_mtu;
		}
		::close(fd);
	}

#ifdef HAVE_LIBLOG4CXX
	LOG4CXX_ERROR(logger,"Can not get MTU of device:" << name.c_str());
#else
	std::cerr << "Can not get MUT of device:" << name.c_str() << std::endl;
#endif
	return 0;
}

void PacketDispatcher::open_device(const std::string &device) {

	char errorbuf[PCAP_ERRBUF_SIZE];
#ifdef __FREEBSD__
	int timeout = 1000; // miliseconds
#else
	int timeout = -1;
#endif

	pcap_ = pcap_open_live(device.c_str(), PACKET_RECVBUFSIZE, 0, timeout, errorbuf);
	if (pcap_ == nullptr) {
#ifdef HAVE_LIBLOG4CXX
		LOG4CXX_ERROR(logger,"Device:" <<device.c_str() << " error:" << errorbuf );
#else
		std::cerr << "Device:" << device.c_str() << " error:" << errorbuf << std::endl;
#endif
		device_is_ready_ = false;
		exit(-1);
		return;
	}
	int ifd = pcap_get_selectable_fd(pcap_);
	if (pcap_setnonblock(pcap_, 1, errorbuf) == 1) {
		device_is_ready_ = false;
		return;
	}
	stream_ = PcapStreamPtr(new PcapStream(io_service_));
			
	stream_->assign(::dup(ifd));
	device_is_ready_ = true;
	input_name_ = device;
}

void PacketDispatcher::close_device(void) {

	if (device_is_ready_) {
		stream_->close();
		pcap_close(pcap_);
		device_is_ready_ = false;
	}
}

void PacketDispatcher::open_pcap_file(const std::string &filename) {

	char errorbuf[PCAP_ERRBUF_SIZE];

        pcap_ = pcap_open_offline(filename.c_str(),errorbuf);
        if (pcap_ == nullptr) {
		pcap_file_ready_ = false;
#ifdef HAVE_LIBLOG4CXX
		LOG4CXX_ERROR(logger,"Unknown pcapfile:" << filename.c_str());
#else
		std::cerr << "Unkown pcapfile:" << filename.c_str() << std::endl;
#endif
		exit(-1);
	} else {
		pcap_file_ready_ = true;
		input_name_ = filename;
	}
}

void PacketDispatcher::close_pcap_file(void) {

	if (pcap_file_ready_) {
		pcap_close(pcap_);
		pcap_file_ready_ = false;
	}
}

void PacketDispatcher::read_network(boost::system::error_code ec) {

	int len = pcap_next_ex(pcap_,&header_,&pkt_data_);
	if (len >= 0) {
		forward_raw_packet((unsigned char*)pkt_data_,header_->len,header_->ts.tv_sec);
	}

// This prevents a problem on the boost asio signal
// remove this if when boost will be bigger than 1.50
#ifdef PYTHON_BINDING
#if BOOST_VERSION >= 104800 && BOOST_VERSION < 105000
	if (PyErr_CheckSignals() == -1) {
		std::cout << "Throwing exception from python" << std::endl;
		throw std::runtime_error("Python exception\n");
       	}
#endif
#endif

	if (!ec || ec == boost::asio::error::would_block)
      		start_read_network();
	// else error but not handler
}

void PacketDispatcher::forward_raw_packet(unsigned char *packet, int length, time_t packet_time) {

	++total_packets_;
	total_bytes_ += length;

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
	stats_.packet_time = packet_time;
#endif
	if (defMux_) {
		current_packet_.setPayload(packet);
		current_packet_.setPayloadLength(length);
		current_packet_.setPrevHeaderSize(0);
		current_packet_.setPacketTime(packet_time);
		current_packet_.setEvidence(false);

		if (defMux_->acceptPacket(current_packet_)) {
			defMux_->setPacket(&current_packet_);
			defMux_->setNextProtocolIdentifier(eth_->getEthernetType());
			defMux_->forwardPacket(current_packet_);
			if ((have_evidences_)and(current_packet_.haveEvidence())) {
				em_->write(current_packet_);
			}
                }
	}
}

void PacketDispatcher::start_read_network(void) {

	read_in_progress_ = false;
	if (!read_in_progress_) {
		read_in_progress_ = true;

		stream_->async_read_some(boost::asio::null_buffers(),
                	boost::bind(&PacketDispatcher::read_network, this,
                                boost::asio::placeholders::error));
	}
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
void PacketDispatcher::start_read_user_input(void) {

	user_shell_->readUserInput();
	
}
#endif

#if defined(STAND_ALONE_TEST) || defined(TESTING)
void PacketDispatcher::setMaxPackets(int packets) {

	max_packets_ = packets;
}
#endif


void PacketDispatcher::run_pcap(void) {

        std::ostringstream msg;
        msg << "Processing packets from file " << input_name_.c_str();
       	info_message(msg.str());

	if (eth_) eth_->setMaxEthernetLength(ETHER_MAX_LEN * 4); // Increase the size to a big value probably 65243 is the best

	if (current_network_stack_) {
		int64_t memory = current_network_stack_->getAllocatedMemory();
		std::string unit = "Bytes";

		unitConverter(memory,unit);
	
		msg.clear();
		msg.str("");
        	msg << "Stack '" << stack_name_ << "' using " << memory << " " << unit << " of memory";
       		info_message(msg.str());
	}
	status_ = PacketDispatcherStatus::RUNNING;
	while (pcap_next_ex(pcap_, &header_, &pkt_data_) >= 0) {
		// Friendly remminder:
		//     header_->len contains length this packet (off wire)
		//     header_->caplen length of portion present	
		forward_raw_packet((unsigned char*)pkt_data_,header_->caplen,header_->ts.tv_sec);
#if defined(STAND_ALONE_TEST) || defined(TESTING)
     		if (total_packets_ >= max_packets_) {
			break;
		} 
#endif
	}
	status_ = PacketDispatcherStatus::STOP;
}


void PacketDispatcher::run_device(void) {

	if (device_is_ready_) {

        	std::ostringstream msg;
        	msg << "Processing packets from device " << input_name_.c_str();

        	info_message(msg.str());

		eth_->setMaxEthernetLength(get_mtu_of_network_device(input_name_));

        	if (current_network_stack_) {
                	int64_t memory = current_network_stack_->getAllocatedMemory();
                	std::string unit = "Bytes";

                	unitConverter(memory,unit);

                	msg.clear();
                	msg.str("");
                	msg << "Stack '" << stack_name_ << "' using " << memory << " " << unit << " of memory";
                	info_message(msg.str());
        	}

		try {
			status_ = PacketDispatcherStatus::RUNNING;
			start_read_network();
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
			start_read_user_input();
#endif
			io_service_.run();
		} catch (const std::exception& e) {
        		std::cerr << __FILE__ << ":" << __func__ << ":ERROR:" << e.what() << std::endl;
			std::cerr << boost::diagnostic_information(e) << std::endl;
        	}
		status_ = PacketDispatcherStatus::STOP;
	} else {

                std::ostringstream msg;
                msg << "The device is not ready to run";

                info_message(msg.str());
	}
}

void PacketDispatcher::open(const std::string &source) {

	std::ifstream infile(source);

	device_is_ready_ = false;
	pcap_file_ready_ = false;

	if (infile.good()) { // The source is a file
		open_pcap_file(source);
	} else {
		open_device(source);
	}
}

void PacketDispatcher::run(void) {

	if (device_is_ready_) {
		run_device();
	} else {
		if (pcap_file_ready_) {
			run_pcap();
		}
	}
}

void PacketDispatcher::close(void) {

        if (device_is_ready_) {
                close_device();
        } else {
                if (pcap_file_ready_) {
                        close_pcap_file();
                }
        }
}

void PacketDispatcher::setPcapFilter(const char *filter) {

	if ((device_is_ready_)or(pcap_file_ready_)) {
		struct bpf_program fp;
		char *c_filter = const_cast<char*>(filter);

		if (pcap_compile(pcap_, &fp, c_filter, 1, PCAP_NETMASK_UNKNOWN) == 0) {

			pcap_filter_ = filter;			
			if (pcap_setfilter(pcap_,&fp) == 0) {
				std::ostringstream msg;
                		msg << "Pcap filter set:" << filter;

                		info_message(msg.str());
			}
		} else {
			std::cerr << "Wrong pcap filter" << std::endl;
		}
		pcap_freecode (&fp);
	}
}


void PacketDispatcher::setEvidences(bool value) {

        if ((!have_evidences_)and(value)) {
                have_evidences_ = true;
                em_->enable();
        } else if ((have_evidences_)and(!value)) {
                have_evidences_ = false;
                em_->disable();
        }
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) 

void PacketDispatcher::setShell(bool enable) {

        user_shell_->setShell(enable);
}

bool PacketDispatcher::getShell() const {

        return user_shell_->getShell();
}

#endif

#if defined(LUA_BINDING)

void PacketDispatcher::setShell(lua_State *L, bool enable) {

        user_shell_->setShell(enable);
	user_shell_->setLuaInterpreter(L);
}

bool PacketDispatcher::getShell() const {

        return user_shell_->getShell();
}

#endif

#if defined(PYTHON_BINDING)
void PacketDispatcher::addTimer(PyObject *callback, int seconds) {

	tm_->addTimer(callback, seconds);
}
#elif defined(RUBY_BINDING)
void PacketDispatcher::addTimer(VALUE callback, int seconds) {

	tm_->addTimer(callback, seconds);
}
#elif defined(LUA_BINDING)
void PacketDispatcher::addTimer(lua_State* L, const char *callback, int seconds) {

	tm_->addTimer(L, callback, seconds);
}
#endif

#if defined(PYTHON_BINDING)

void PacketDispatcher::setStack(boost::python::object &stack) {

	if (stack.is_none()) {
		// The user sends a Py_None 
		pystack_ = boost::python::object();
		stack_name_ = "None";
        	defMux_.reset();
	} else {
		boost::python::extract<SharedPointer<NetworkStack>> extractor(stack);

        	if (extractor.check()) {
        		SharedPointer<NetworkStack> pstack = extractor();
                	pystack_ = stack;
                
			// The NetworkStack have been extracted and now call the setStack method
                	setStack(pstack);
        	} else {
			std::cerr << "Can not extract NetworkStack from python object" << std::endl;
		}
	}
}

PacketDispatcher& PacketDispatcher::__enter__() {

	open(input_name_);
        return *this;
}

bool PacketDispatcher::__exit__(boost::python::object type, boost::python::object val, boost::python::object traceback) {

	close();
        return type.ptr() == Py_None;
}

void PacketDispatcher::forwardPacket(const std::string &packet, int length) {

	const unsigned char *pkt = reinterpret_cast<const unsigned char *>(packet.c_str());

	// TODO: pass the time to the method forward_raw_packet from the
	// python binding
	forward_raw_packet((unsigned char*)pkt, length, 0);
	return;
}

const char *PacketDispatcher::getStatus() const {

        if (status_ == PacketDispatcherStatus::RUNNING)
                return "running";
        else
                return "stoped";
}

#endif

std::ostream& operator<< (std::ostream &out, const PacketDispatcher &pd) {

	out << std::setfill(' ');
	out << "PacketDispatcher(" << &pd <<") statistics" << std::endl;
	out << "\t" << "Connected to " << pd.stack_name_ <<std::endl;
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)

        pd.tm_->statistics(out);

	if ((pd.device_is_ready_) and (pd.user_shell_->getShell())) {
		// Compute the number of packets per second and bytes.
		int seconds = difftime(pd.stats_.packet_time, pd.stats_.last_packet_time);
		int64_t packets_per_second = pd.total_packets_ - pd.stats_.last_total_packets_sample;
		int64_t bytes_per_second = pd.total_bytes_ - pd.stats_.last_total_bytes_sample;

		if (seconds > 0 ) {
			packets_per_second = packets_per_second / seconds;
			bytes_per_second = bytes_per_second / seconds;
		}

		pd.stats_.last_packet_time = pd.stats_.packet_time; // update the last time we make the compute
		pd.stats_.last_total_packets_sample = pd.total_packets_;
		pd.stats_.last_total_bytes_sample = pd.total_bytes_;

		out << "\t" << "Total packets/sec:      " << std::dec << std::setw(10) << packets_per_second <<std::endl;
		out << "\t" << "Total bytes/sec:    " << std::dec << std::setw(14) << bytes_per_second <<std::endl;
	}
#endif
	if (pd.pcap_filter_.length() > 0) {
		out << "\t" << "Pcap filter:" << pd.pcap_filter_ <<std::endl;
	}
	out << "\t" << "Total packets:          " << std::dec << std::setw(10) << pd.total_packets_ <<std::endl;
	out << "\t" << "Total bytes:        " << std::dec << std::setw(14) << pd.total_bytes_ <<std::endl;

        return out;
}

void PacketDispatcher::status(void) {

	std::ostringstream msg;
        msg << "PacketDispatcher ";
	if (status_ == PacketDispatcherStatus::RUNNING)
		msg << "running";
	else
		msg << "stoped";
	msg << ", plug to " << stack_name_;
	msg << ", packets " << total_packets_ << ", bytes " << total_bytes_;

        info_message(msg.str());
}

void PacketDispatcher::showCurrentPayloadPacket(std::basic_ostream<char> &out) {

	if ((device_is_ready_)or(pcap_file_ready_)) {
		if (current_network_stack_) {	
			std::tuple<Flow*,Flow*> flows = current_network_stack_->getCurrentFlows();
			Flow *low_flow = std::get<0>(flows);
			Flow *high_flow = std::get<1>(flows);

			if (low_flow) out << "\tFlow:" << *low_flow << std::endl;
			if (high_flow) out << "\tFlow:" << *high_flow << std::endl;
		}
		if ((pkt_data_) and (header_))
			showPayload(out,pkt_data_,header_->caplen);
		out << std::dec;
	}
}

void PacketDispatcher::showCurrentPayloadPacket() { showCurrentPayloadPacket(std::cout); }

} // namespace aiengine
