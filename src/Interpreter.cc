/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "Interpreter.h"

namespace aiengine {

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)

Interpreter::Interpreter(boost::asio::io_service &io_service_, int fd):
	user_input_(io_service_, ::dup(fd)),
	user_input_buffer_(MaxInputBuffer),
	shell_enable_(false),
	want_exit_(false),
	in_code_block_(false),
	current_prompt_(const_cast<char*>(Prompt)),
	cmd_("") {
#if defined(LUA_BINDING)
	L_ = nullptr;
#endif
}

void Interpreter::setShell(bool enable) {

	if (shell_enable_) {
		if (!enable) {
			stop();
		}
	} else {
		if (enable) {
			start();
		}
	}
}

void Interpreter::start() {

	shell_enable_ = true;
#if defined(PYTHON_BINDING) 
	const char *interpreter_banner = "Python";
#elif defined(RUBY_BINDING)
	const char *interpreter_banner = "Ruby";
#elif  defined(LUA_BINDING)
	const char *interpreter_banner = "Lua";
#endif
        std::chrono::system_clock::time_point time_point = std::chrono::system_clock::now();
        std::time_t now = std::chrono::system_clock::to_time_t(time_point);
#ifdef __clang__
        std::cout << "[" << std::put_time(std::localtime(&now), "%D %X") << "] ";
#else
        char mbstr[100];
        std::strftime(mbstr, 100, "%D %X", std::localtime(&now));
        std::cout << "[" << mbstr << "] ";
#endif
	std::cout << interpreter_banner << " AIEngine " << VERSION << " shell enable" << std::endl;
}

void Interpreter::stop() {
       
        shell_enable_ = false;
        user_input_buffer_.consume(MaxInputBuffer);

	std::chrono::system_clock::time_point time_point = std::chrono::system_clock::now();
        std::time_t now = std::chrono::system_clock::to_time_t(time_point);
#ifdef __clang__
        std::cout << "[" << std::put_time(std::localtime(&now), "%D %X") << "] ";
#else
        char mbstr[100];
        std::strftime(mbstr, 100, "%D %X", std::localtime(&now));
        std::cout << "[" << mbstr << "] ";
#endif
	std::cout << "exiting AIEngine " << VERSION << " shell disable" << std::endl;
	user_input_.close();
}

void Interpreter::readUserInput() {

	if (shell_enable_) {
        	boost::asio::async_read_until(user_input_, user_input_buffer_, '\n',
                	boost::bind(&Interpreter::handle_read_user_input, this,
                        boost::asio::placeholders::error));
        }
}


bool has_suffix(const std::string &str, const std::string &suffix)
{
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

void Interpreter::execute_user_command(const std::string& cmd) {

#if defined(PYTHON_BINDING)

	// Verify if there is : at the end
	if (has_suffix(cmd,":")) {
		in_code_block_ = true;
		current_prompt_ = const_cast<char*>(CodePrompt);
		cmd_ += "\n" + cmd;
		return;
	} else {
		if (in_code_block_) {
			if (cmd.length() == 0) {
				current_prompt_ = const_cast<char*>(Prompt);
				in_code_block_ = false;
			} else {
				cmd_ += "\n" + cmd;
				return;
			}
		} else {
			cmd_ = cmd;
		}
	}

	try {
		PyGilContext gil_lock();

		// Retrieve the main module.
		boost::python::object main = boost::python::import("__main__");
  		// Retrieve the main module's namespace
  		boost::python::object global(main.attr("__dict__"));

		boost::python::exec(cmd_.c_str(), global);
	} catch (boost::python::error_already_set const &) {
		PyErr_Print();
	}

	cmd_.clear();

#elif defined(RUBY_BINDING)

	if (cmd.empty())
		return;
	
	int state = 0;
	rb_eval_string_protect(cmd.c_str(), &state);
	if (state) {
		rb_raise(rb_eRuntimeError, "Error");
	}
#elif defined(LUA_BINDING)
	if (cmd.empty())
		return;

	int ret = luaL_dostring(L_, cmd.c_str());
	if (ret != 0) {
		std::cout << "ERROR:" << lua_tostring(L_, -1) << std::endl; 
	}
#endif

}

void Interpreter::handle_read_user_input(boost::system::error_code error) {

	if ((!error)and(shell_enable_)) {
		std::istream user_stream(&user_input_buffer_);
                std::string cmd;

		std::getline(user_stream, cmd);

		if (want_exit_) {
			// The users type yes
			if (cmd.compare("yes") == 0) {
				stop();
				return;
			}	
			want_exit_ = false;
		} else {
			// The user wants to exist from the shell	
			if (cmd.compare("quit()") == 0) {
				std::cout << "Are you sure? (yes/no)" << std::flush;
               			user_input_buffer_.consume(MaxInputBuffer);
				want_exit_ = true;
				readUserInput();
				return;
			} else {
               			execute_user_command(cmd);	
			}
		}
		user_input_buffer_.consume(MaxInputBuffer);
		std::cout << current_prompt_ ;
		std::cout.flush();
		readUserInput();
	} 
}

#endif

} // namespace aiengine 



