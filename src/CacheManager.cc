/*
 * AIEngine a new generation network intrusion detection system. 
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "CacheManager.h"

namespace aiengine {

CacheManager::CacheManager():
	http_info_cache_(),
	ssl_info_cache_(),
	sip_info_cache_(),
	gprs_info_cache_(),
	tcp_info_cache_(),
	smtp_info_cache_(),
	imap_info_cache_(),
	pop_info_cache_(),
	dns_info_cache_(),
	ssdp_info_cache_(),
	bitcoin_info_cache_(),
	coap_info_cache_(),
	mqtt_info_cache_(),
	netbios_info_cache_(),
	dhcp_info_cache_(),
	dhcpv6_info_cache_(),
	smb_info_cache_(),
	ssh_info_cache_(),
	dcerpc_info_cache_(),
	freq_info_cache_(),
        packet_freq_info_cache_() 
	{}

void CacheManager::release_frequencies(Flow *flow) {

	SharedPointer<Frequencies> freq = flow->frequencies;
	if ((freq) and (freq_info_cache_)) {
		freq_info_cache_->release(freq);
	}

	SharedPointer<PacketFrequencies> pkt_freq = flow->packet_frequencies;
	if ((pkt_freq) and (packet_freq_info_cache_)) {
		packet_freq_info_cache_->release(pkt_freq);
	}
}

void CacheManager::releaseFlow(Flow *flow) {

	if (flow->getProtocol() == IPPROTO_TCP) {
		releaseTCPFlow(flow);
	} else {
		releaseUDPFlow(flow);
	}
	release_frequencies(flow);
}

void CacheManager::releaseSMTPInfo(const SharedPointer<SMTPInfo> &info) {

	if (smtp_info_cache_)
		smtp_info_cache_->release(info);
}

void CacheManager::releaseIMAPInfo(const SharedPointer<IMAPInfo> &info) {

	if (imap_info_cache_)
		imap_info_cache_->release(info);
}

void CacheManager::releasePOPInfo(const SharedPointer<POPInfo> &info) {

	if (pop_info_cache_)
		pop_info_cache_->release(info);
}

void CacheManager::releaseTCPFlow(Flow *flow) {

	SharedPointer<TCPInfo> tcpinfo = flow->getTCPInfo();
	if ((tcpinfo)and(tcp_info_cache_)) {
		tcp_info_cache_->release(tcpinfo);	
	}

	if (auto httpinfo = flow->getHTTPInfo()) {
		if (http_info_cache_) 
			http_info_cache_->release(httpinfo); 
	} else if (auto sslinfo = flow->getSSLInfo()) {
		if (ssl_info_cache_) 
			ssl_info_cache_->release(sslinfo);
	} else if (auto smtpinfo = flow->getSMTPInfo()) {
		if (smtp_info_cache_) 
			smtp_info_cache_->release(smtpinfo);
	} else if (auto popinfo = flow->getPOPInfo()) {
		if (pop_info_cache_) 
			pop_info_cache_->release(popinfo);
	} else if (auto imapinfo = flow->getIMAPInfo()) {
		if (imap_info_cache_) 
			imap_info_cache_->release(imapinfo);
	} else if (auto btinfo = flow->getBitcoinInfo()) {
		if (bitcoin_info_cache_) 
			bitcoin_info_cache_->release(btinfo);
	} else if (auto minfo = flow->getMQTTInfo()) {
		if (mqtt_info_cache_) 
			mqtt_info_cache_->release(minfo);
	} else if (auto sinfo = flow->getSMBInfo()) {
		if (smb_info_cache_) 
			smb_info_cache_->release(sinfo);
	} else if (auto sinfo = flow->getSSHInfo()) {
		if (ssh_info_cache_) 
			ssh_info_cache_->release(sinfo);
	} else if (auto dinfo = flow->getDCERPCInfo()) {
		if (dcerpc_info_cache_) 
			dcerpc_info_cache_->release(dinfo);
	}
}

void CacheManager::releaseUDPFlow(Flow *flow) {

	if (flow->layer4info) {
		if (gprs_info_cache_)
			gprs_info_cache_->release(flow->getGPRSInfo());
	} 
	if (auto dnsinfo = flow->getDNSInfo()) {
		if (dns_info_cache_) 
			dns_info_cache_->release(dnsinfo);
	} else if (auto sipinfo = flow->getSIPInfo()) {
		if (sip_info_cache_) 
			sip_info_cache_->release(sipinfo);
	} else if (auto ssdpinfo = flow->getSSDPInfo()) {
		if (ssdp_info_cache_) 
			ssdp_info_cache_->release(ssdpinfo);
	} else if (auto nbinfo = flow->getNetbiosInfo()) {
		if (netbios_info_cache_) 
			netbios_info_cache_->release(nbinfo);
	} else if (auto coapinfo = flow->getCoAPInfo()) {
		if (coap_info_cache_) 
			coap_info_cache_->release(coapinfo);
	} else if (auto dhcpinfo = flow->getDHCPInfo()) {
		if (dhcp_info_cache_) 
			dhcp_info_cache_->release(dhcpinfo);
	} else if (auto dhcpv6info = flow->getDHCPv6Info()) {
		if (dhcpv6_info_cache_) 
			dhcpv6_info_cache_->release(dhcpv6info);
	}
}

} // namespace
