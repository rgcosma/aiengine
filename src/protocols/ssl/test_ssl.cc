/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_ssl.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE ssltest
#endif
#include <boost/test/unit_test.hpp>

BOOST_FIXTURE_TEST_SUITE(ssl_suite_static, StackSSLtest)

BOOST_AUTO_TEST_CASE (test01)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length = raw_packet_ethernet_ip_tcp_ssl_client_hello_length;
        Packet packet(pkt, length);

	inject(packet);

	// Check the results
	BOOST_CHECK(ip->getTotalPackets() == 1);
	BOOST_CHECK(ip->getTotalValidPackets() == 1);
	BOOST_CHECK(ip->getTotalBytes() == 245);
	BOOST_CHECK(ip->getTotalInvalidPackets() == 0);

	// tcp
	BOOST_CHECK(tcp->getTotalPackets() == 1);
	BOOST_CHECK(tcp->getTotalBytes() == 225);
	BOOST_CHECK(tcp->getTotalInvalidPackets() == 0);

	// ssl
	BOOST_CHECK(ssl->getTotalPackets() == 1);
	BOOST_CHECK(ssl->getTotalValidPackets() == 1);
	BOOST_CHECK(ssl->getTotalInvalidPackets() == 0);
	BOOST_CHECK(ssl->getTotalBytes() == 193);
	BOOST_CHECK(ssl->getTotalInvalidPackets() == 0);

	// all this counters are zero because there is no memory for ssl
	BOOST_CHECK(ssl->getTotalHandshakes() == 0);
        BOOST_CHECK(ssl->getTotalClientHellos() == 0);
        BOOST_CHECK(ssl->getTotalServerHellos() == 0);
        BOOST_CHECK(ssl->getTotalCertificates() == 0);

	BOOST_CHECK(ssl->getTotalEvents() == 0);

	BOOST_CHECK(ssl->processPacket(packet) == true);
}

BOOST_AUTO_TEST_CASE (test02)
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length1 = raw_packet_ethernet_ip_tcp_ssl_client_hello_length;
        Packet packet1(pkt1, length1);

	ssl->increaseAllocatedMemory(2);

	inject(packet1);

	BOOST_CHECK(ssl->getTotalHandshakes() == 1);
        BOOST_CHECK(ssl->getTotalClientHellos() == 1);
        BOOST_CHECK(ssl->getTotalServerHellos() == 0);
        BOOST_CHECK(ssl->getTotalCertificates() == 0);
        BOOST_CHECK(ssl->getTotalRecords() == 1);

        unsigned char *pkt2 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello_2);
        int length2 = raw_packet_ethernet_ip_tcp_ssl_client_hello_2_length;
        Packet packet2(pkt2, length2);

	inject(packet2);

        // Check the results
        BOOST_CHECK(ssl->getTotalClientHellos() == 2);
        BOOST_CHECK(ssl->getTotalServerHellos() == 0);
        BOOST_CHECK(ssl->getTotalCertificates() == 0);
        BOOST_CHECK(ssl->getTotalRecords() == 2);
	BOOST_CHECK(ssl->getTotalEvents() == 0);
}

BOOST_AUTO_TEST_CASE (test03)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_tor);
        int length = raw_packet_ethernet_ip_tcp_ssl_tor_length;
        Packet packet(pkt, length);

        ssl->increaseAllocatedMemory(1);

	inject(packet);

        BOOST_CHECK(ssl->getTotalPackets() == 1);
        BOOST_CHECK(ssl->getTotalValidPackets() == 1);
        BOOST_CHECK(ssl->getTotalInvalidPackets() == 0);
        BOOST_CHECK(ssl->getTotalBytes() == 923);
        BOOST_CHECK(ssl->getTotalInvalidPackets() == 0);

        // Check the results
        BOOST_CHECK(ssl->getTotalClientHellos() == 0);
        BOOST_CHECK(ssl->getTotalServerHellos() == 1);
        BOOST_CHECK(ssl->getTotalCertificates() == 1);
        BOOST_CHECK(ssl->getTotalRecords() == 4); // The packet contains 4 records, but we only process 3 types;
}

BOOST_AUTO_TEST_CASE (test04)
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length1 = raw_packet_ethernet_ip_tcp_ssl_client_hello_length;
        Packet packet1(pkt1, length1);

        SharedPointer<Flow> flow = SharedPointer<Flow>(new Flow());

        ssl->increaseAllocatedMemory(0);

        flow->packet = const_cast<Packet*>(&packet1);
        ssl->processFlow(flow.get());

        BOOST_CHECK(flow->layer7info == nullptr);
}

BOOST_AUTO_TEST_CASE (test05)
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (&(raw_packet_ethernet_ip_tcp_ssl_client_hello[66]));
        int length1 = raw_packet_ethernet_ip_tcp_ssl_client_hello_length - 66;
        Packet packet1(pkt1, length1);

        SharedPointer<Flow> flow = SharedPointer<Flow>(new Flow());

        ssl->increaseAllocatedMemory(1);

        flow->packet = const_cast<Packet*>(&packet1);
        ssl->processFlow(flow.get());

        BOOST_CHECK(flow->layer7info != nullptr);
	std::string cad("0.drive.google.com");

	SharedPointer<SSLInfo> info = flow->getSSLInfo();
	BOOST_CHECK(info != nullptr);
	// The host is valid
        BOOST_CHECK(cad.compare(info->host_name->getName()) == 0);
	BOOST_CHECK(info->getHeartbeat() == false);
	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
	BOOST_CHECK(info->getCipher() == 0); // The client dont set the cipher
}

BOOST_AUTO_TEST_CASE (test06)
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (&(raw_packet_ethernet_ip_tcp_ssl_client_hello_2[54]));
        int length1 = raw_packet_ethernet_ip_tcp_ssl_client_hello_2_length - 54;
        Packet packet1(pkt1, length1);

        auto flow = SharedPointer<Flow>(new Flow());

        ssl->increaseAllocatedMemory(1);

        flow->packet = const_cast<Packet*>(&packet1);
        ssl->processFlow(flow.get());

        std::string cad("atv-ps.amazon.com");

	SharedPointer<SSLInfo> info = flow->getSSLInfo();
	BOOST_CHECK(info != nullptr);
	BOOST_CHECK(info->host_name != nullptr);
        // The host is valid
        BOOST_CHECK(cad.compare(info->host_name->getName()) == 0);
	BOOST_CHECK(info->getHeartbeat() == false);
	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
}

// Tor ssl case 
BOOST_AUTO_TEST_CASE (test07)
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (&(raw_packet_ethernet_ip_tcp_ssl_tor_hello[54]));
        int length1 = raw_packet_ethernet_ip_tcp_ssl_tor_hello_length - 54;
        Packet packet1(pkt1, length1);

        auto flow = SharedPointer<Flow>(new Flow());

        ssl->increaseAllocatedMemory(1);

        flow->packet = const_cast<Packet*>(&packet1);
        ssl->processFlow(flow.get());

        std::string cad("www.6k6fnxstu.com");

        // The host is valid
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
	BOOST_CHECK(info != nullptr);
        BOOST_CHECK(cad.compare(info->host_name->getName()) == 0);
	BOOST_CHECK(info->getHeartbeat() == false);
	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
}

BOOST_AUTO_TEST_CASE (test08)
{
        auto host_mng = SharedPointer<DomainNameManager>(new DomainNameManager());
        auto host_name = SharedPointer<DomainName>(new DomainName("example",".drive.google.com"));

        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length = raw_packet_ethernet_ip_tcp_ssl_client_hello_length;
        Packet packet(pkt, length);

        ssl->increaseAllocatedMemory(1);
        ssl->setDomainNameManager(host_mng);
        host_mng->addDomainName(host_name);
        
	mux_eth->setPacket(&packet);
        eth->setHeader(mux_eth->getCurrentPacket()->getPayload());
        mux_eth->setNextProtocolIdentifier(eth->getEthernetType());
        mux_eth->forwardPacket(packet);

	BOOST_CHECK(host_name->getMatchs() == 1);

	BOOST_CHECK(ssl->getTotalAllowHosts() == 1);
	BOOST_CHECK(ssl->getTotalBanHosts() == 0);
	BOOST_CHECK(ssl->getTotalEvents() == 1);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK( flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
	BOOST_CHECK( info != nullptr);
	
	flow->setLabel("I like this");

        std::filebuf fb;
        fb.open ("/dev/null",std::ios::out);
        std::ostream outp(&fb);
        flow->serialize(outp);
        flow->showFlowInfo(outp);
        outp << *(info.get());
        fb.close();
      
	JsonFlow j;
	info->serialize(j); 

	std::string protocol("SSLProtocol");
	BOOST_CHECK(protocol.compare(flow->getL7ProtocolName()) == 0);
	std::string label("I like this");
	BOOST_CHECK(label.compare(flow->getLabel()) == 0);

	// Force the cache
	releaseFlow(flow);
 
	ssl->setDomainNameManager(nullptr);
}

BOOST_AUTO_TEST_CASE (test09)
{
        auto host_mng = SharedPointer<DomainNameManager>(new DomainNameManager());
        auto host_name = SharedPointer<DomainName>(new DomainName("example",".paco.google.com"));

        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length = raw_packet_ethernet_ip_tcp_ssl_client_hello_length;
        Packet packet(pkt, length);

        ssl->increaseAllocatedMemory(1);
        ssl->setDomainNameManager(host_mng);
        host_mng->addDomainName(host_name);

	inject(packet);

        BOOST_CHECK(host_name->getMatchs() == 0);
}

BOOST_AUTO_TEST_CASE (test10)
{
        auto host_mng = SharedPointer<DomainNameManager>(new DomainNameManager());
        auto host_name = SharedPointer<DomainName>(new DomainName("example",".google.com"));

        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length = raw_packet_ethernet_ip_tcp_ssl_client_hello_length;
        Packet packet(pkt, length);

        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (&(raw_packet_ethernet_ip_tcp_ssl_tor_hello[54]));
        int length1 = raw_packet_ethernet_ip_tcp_ssl_tor_hello_length - 54;
        Packet packet1(pkt1, length1);

        ssl->increaseAllocatedMemory(1);
        ssl->setDomainNameBanManager(host_mng);
        host_mng->addDomainName(host_name);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK(flow != nullptr);

	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->isBanned() == true);
        BOOST_CHECK(info->host_name  == nullptr);
	
        BOOST_CHECK(host_name->getMatchs() == 1);

        BOOST_CHECK(ssl->getTotalAllowHosts() == 0);
        BOOST_CHECK(ssl->getTotalBanHosts() == 1);
	BOOST_CHECK(ssl->getTotalEvents() == 0);

        flow->packet = const_cast<Packet*>(&packet1);
	// Inject the same flow 
	ssl->processFlow(flow);

        BOOST_CHECK(info->isBanned() == true);
        BOOST_CHECK(info->host_name  == nullptr);
}

BOOST_AUTO_TEST_CASE (test11)
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length1 = raw_packet_ethernet_ip_tcp_ssl_client_hello_length;
        Packet packet1(pkt1, length1);

        unsigned char *pkt2 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_tor_hello);
        int length2 = raw_packet_ethernet_ip_tcp_ssl_tor_hello_length;
        Packet packet2(pkt2,length2);

        ssl->increaseAllocatedMemory(2);

	inject(packet1);
	inject(packet2);

	auto fm = tcp->getFlowManager();

#if defined(STAND_ALONE)
        Cache<StringCache>::CachePtr c = ssl->getHostCache();

	BOOST_CHECK(c->getTotal() == 0);
	BOOST_CHECK(c->getTotalAcquires() == 2);
	BOOST_CHECK(c->getTotalReleases() == 0);
#endif
	for (auto &f: fm->getFlowTable()) {
		BOOST_CHECK(f->getSSLInfo() != nullptr);
	}
	ssl->releaseCache();

#if defined(STAND_ALONE)
	BOOST_CHECK(c->getTotal() == 2);
	BOOST_CHECK(c->getTotalAcquires() == 2);
	BOOST_CHECK(c->getTotalReleases() == 2);
#endif

	for (auto &f: fm->getFlowTable()) {
		BOOST_CHECK(f->layer7info == nullptr);
	}
}

// have a renegotitaion header on the ssl hello
BOOST_AUTO_TEST_CASE (test12)
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (&(raw_packet_ethernet_ip_tcp_ssl_renegotiation[54]));
        int length1 = raw_packet_ethernet_ip_tcp_ssl_renegotiation_length - 54;
        Packet packet1(pkt1, length1);

        auto flow = SharedPointer<Flow>(new Flow());

        ssl->increaseAllocatedMemory(1);

        flow->packet = const_cast<Packet*>(&packet1);
        ssl->processFlow(flow.get());

        std::string cad("ipv4_1-aaag0-c001.1.000001.xx.aaaavideo.net");

        // The host is valid
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
	BOOST_CHECK(info != nullptr);
        BOOST_CHECK(cad.compare(info->host_name->getName()) == 0);
        BOOST_CHECK(info->getHeartbeat() == false);
}

// have a heartbeat header on the ssl hello, just at the end
BOOST_AUTO_TEST_CASE (test13)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&(raw_ethernet_ip_tcp_port_8080_ssl_client_hello_heartbeat[66]));
        int length = raw_ethernet_ip_tcp_port_8080_ssl_client_hello_heartbeat_length - 66;
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

        ssl->increaseAllocatedMemory(1);

        flow->packet = const_cast<Packet*>(&packet);
        ssl->processFlow(flow.get());

        std::string cad("www.gi7n35abj6dehjlg5g7.com");

        // The host is valid
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
	BOOST_CHECK(info != nullptr);
        BOOST_CHECK(cad.compare(info->host_name->getName()) == 0);
        BOOST_CHECK(info->getHeartbeat() == true);
	
	CounterMap c = ssl->getCounters();

	// Set the alert just for execute the code
	info->setAlert(true);
	info->setAlertCode(10);

        JsonFlow j;
        info->serialize(j);
}

BOOST_AUTO_TEST_CASE (test14) // Corrupt hello length to verify anomaly
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length = raw_packet_ethernet_ip_tcp_ssl_client_hello_length - 16;
        Packet packet(pkt, length);

        ssl->increaseAllocatedMemory(1);

        inject(packet);

	Flow *flow = ssl->getCurrentFlow();
	BOOST_CHECK(flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
	BOOST_CHECK(info != nullptr);

	BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::SSL_BOGUS_HEADER);

        BOOST_CHECK(ssl->getTotalEvents() == 1);
        BOOST_CHECK(ssl->getTotalHandshakes() == 0);
        BOOST_CHECK(ssl->getTotalClientHellos() == 0);
        BOOST_CHECK(ssl->getTotalServerHellos() == 0);
        BOOST_CHECK(ssl->getTotalCertificates() == 0);
        BOOST_CHECK(ssl->getTotalRecords() == 0);

	std::string anomaly_str("SSL bogus header");
	BOOST_CHECK(anomaly_str.compare(anomaly->getName(flow->getPacketAnomaly())) == 0);

        std::filebuf fb;
        fb.open ("/dev/null",std::ios::out);
        std::ostream outp(&fb);
        flow->serialize(outp);
        flow->showFlowInfo(outp);
        outp << *(info.get());
        fb.close();
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(ssl_suite_dynamic, StackSSLtest)

BOOST_AUTO_TEST_CASE (test01)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_client_hello);
        int length = raw_packet_ethernet_ip_tcp_ssl_client_hello_length;
        Packet packet(pkt, length);

        ssl->increaseAllocatedMemory(0);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

        inject(packet);

	Flow *flow = ssl->getCurrentFlow();
	BOOST_CHECK(flow != nullptr);

	BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::NONE);

        BOOST_CHECK(flow->layer7info != nullptr);
        BOOST_CHECK(flow->getSSLInfo() != nullptr);

        BOOST_CHECK(ssl->getTotalHandshakes() == 1);
        BOOST_CHECK(ssl->getTotalClientHellos() == 1);
        BOOST_CHECK(ssl->getTotalServerHellos() == 0);
        BOOST_CHECK(ssl->getTotalCertificates() == 0);
        BOOST_CHECK(ssl->getTotalRecords() == 1);
}

BOOST_AUTO_TEST_CASE (test02) // test the ssl alerts
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&(raw_packet_ethernet_ip_tcp_ssl_alert_warning_unrecognized_name[66]));
        int length = raw_packet_ethernet_ip_tcp_ssl_alert_warning_unrecognized_name_length - 66;
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

	flow->total_packets_l7 = 5;

        ssl->increaseAllocatedMemory(0);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

        flow->packet = const_cast<Packet*>(&packet);
        ssl->processFlow(flow.get());

        BOOST_CHECK(flow->layer7info != nullptr);
        auto info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
	BOOST_CHECK(info->isAlert() == true);
	BOOST_CHECK(info->getAlertCode() == 112); // Unrecognized Name
}

BOOST_AUTO_TEST_CASE (test03) // test 4 pdus in one packet
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_handshake_4pdus);
        int length = raw_packet_ethernet_ip_tcp_ssl_handshake_4pdus_length;
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK(flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);

	// Check the cipher, in this case is C030 == TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
	BOOST_CHECK(info->getCipher() == 0xC030);

	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 1);
	BOOST_CHECK(ssl->getTotalRecords() == 4);
}

BOOST_AUTO_TEST_CASE (test04) // corrupt the certificate packet, is the third one
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_handshake_4pdus);
        int length = raw_packet_ethernet_ip_tcp_ssl_handshake_4pdus_length - (9 /* server done*/ + 8);
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK(flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
	
	BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::SSL_BOGUS_HEADER);

	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 2);
}

BOOST_AUTO_TEST_CASE (test05) // three messages on the packet 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_handshake_3pdus);
        int length = raw_packet_ethernet_ip_tcp_ssl_handshake_3pdus_length; 
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK(flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(ssl->getTotalServerHellos() == 0);
	BOOST_CHECK(ssl->getTotalCertificates() == 0);
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 3);
}

BOOST_AUTO_TEST_CASE (test06) // three messages on the packet 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_ssl_handshake_3pdus_2);
        int length = raw_packet_ethernet_ip_tcp_ssl_handshake_3pdus_2_length; 
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK( flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
	BOOST_CHECK(info->getVersion() == TLS1_2_VERSION);

	BOOST_CHECK(ssl->getTotalHandshakes() == 2);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 1);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 0);
	BOOST_CHECK(ssl->getTotalCertificates() == 0);
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 3);
}

BOOST_AUTO_TEST_CASE (test07) // client hello to mtalk.google.com and server 
{
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f1_ssl_tlsv1_2_handshake_client_hello);
        int length1 = raw_packet_ethernet_ip_tcp_f1_ssl_tlsv1_2_handshake_client_hello_length; 
        Packet packet1(pkt1, length1);
        unsigned char *pkt2 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f1_ssl_tlsv1_2_handshake_server_hello);
        int length2 = raw_packet_ethernet_ip_tcp_f1_ssl_tlsv1_2_handshake_server_hello_length; 
        Packet packet2(pkt2, length2);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet1);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK(flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name != nullptr);

	BOOST_CHECK(info->getVersion() == TLS1_VERSION);

        std::string cad("mtalk.google.com");
        BOOST_CHECK(cad.compare(info->host_name->getName()) == 0);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(info->getCipher() == 0);

	BOOST_CHECK(ssl->getTotalHandshakes() == 1);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 1);
	BOOST_CHECK(ssl->getTotalServerHellos() == 0);
	BOOST_CHECK(ssl->getTotalCertificates() == 0);
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 1);
	
	inject(packet2);

	BOOST_CHECK(info->getCipher() == 0xCCA9); // TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256

	// 3 handshakes, server hello and the certificate (partially)	
	BOOST_CHECK(ssl->getTotalHandshakes() == 3);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 1);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 3);
}

BOOST_AUTO_TEST_CASE (test08) // server hello, cert and server done 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f2_ssl_handshake_server_3pdus);
        int length = raw_packet_ethernet_ip_tcp_f2_ssl_handshake_server_3pdus_length; 
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK(flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);

	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);
	BOOST_CHECK(info->getCipher() == 0x0005); // TLS_RSA_WITH_RC4_128_SHA

	BOOST_CHECK(ssl->getTotalHandshakes() == 3);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 1);
	BOOST_CHECK(ssl->getTotalRecords() == 3);
}

BOOST_AUTO_TEST_CASE (test09) // server hello, cert and cert request 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f3_ssl_handshake_server_3pdus);
        int length = raw_packet_ethernet_ip_tcp_f3_ssl_handshake_server_3pdus_length; 
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

        // Check the results
        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 1420); 
        BOOST_CHECK(ip->getTotalInvalidPackets() == 0);

        // tcp
        BOOST_CHECK(tcp->getTotalPackets() == 1);
        BOOST_CHECK(tcp->getTotalBytes() == 1400); 
        BOOST_CHECK(tcp->getTotalInvalidPackets() == 0);

        // ssl
        BOOST_CHECK(ssl->getTotalPackets() == 1);
        BOOST_CHECK(ssl->getTotalValidPackets() == 1);
        BOOST_CHECK(ssl->getTotalInvalidPackets() == 0);
        BOOST_CHECK(ssl->getTotalBytes() == 1380); 
        BOOST_CHECK(ssl->getTotalInvalidPackets() == 0);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK(flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);

	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);
	BOOST_CHECK(info->getCipher() == 0x002F); // TLS_RSA_WITH_AES_128_CBC_SHA

	BOOST_CHECK(ssl->getTotalHandshakes() == 2);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalCertificateRequests() == 0); // Corrupted
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 2);

	BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::SSL_BOGUS_HEADER); // The 3 record is corrupted
}

BOOST_AUTO_TEST_CASE (test10) // client key exchange, cert verify 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f4_ssl_handshake_server_4pdus);
        int length = raw_packet_ethernet_ip_tcp_f4_ssl_handshake_server_4pdus_length; 
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	BOOST_CHECK(flow != nullptr);
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);

	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(ssl->getTotalHandshakes() == 3);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 1);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 0);
	BOOST_CHECK(ssl->getTotalCertificates() == 0);
	BOOST_CHECK(ssl->getTotalCertificateRequests() == 0);
	BOOST_CHECK(ssl->getTotalCertificateVerifies() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 4);
}

BOOST_AUTO_TEST_CASE (test11) // Change cipher specs message 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_f5_ssl_change_cipher_specs[66]);
        int length = raw_packet_ethernet_ip_tcp_f5_ssl_change_cipher_specs_length - 66; 
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());
	
	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

        flow->packet = const_cast<Packet*>(&packet);
        ssl->processFlow(flow.get());

	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);

	// The version should be set on previous packets not on a change cipher spec
	BOOST_CHECK(info->getVersion() == 0);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(ssl->getTotalHandshakes() == 0);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 1);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 0);
	BOOST_CHECK(ssl->getTotalCertificates() == 0);
	BOOST_CHECK(ssl->getTotalCertificateRequests() == 0);
	BOOST_CHECK(ssl->getTotalCertificateVerifies() == 0);
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 1);
}

BOOST_AUTO_TEST_CASE (test12) // Finish handshake
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f5_encrypted_message_finish_hk);
        int length = raw_packet_ethernet_ip_tcp_f5_encrypted_message_finish_hk_length; 
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);

	// The version should be set on previous packets not on a change cipher spec
	BOOST_CHECK(info->getVersion() == TLS1_2_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(ssl->getTotalHandshakes() == 1);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 0);
	BOOST_CHECK(ssl->getTotalCertificates() == 0);
	BOOST_CHECK(ssl->getTotalCertificateRequests() == 0);
	BOOST_CHECK(ssl->getTotalCertificateVerifies() == 0);
	BOOST_CHECK(ssl->getTotalServerDones() == 0);
	BOOST_CHECK(ssl->getTotalRecords() == 1);
}

BOOST_AUTO_TEST_CASE (test13) // split last record in another packet 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f6_server_cert_split);
        int length = raw_packet_ethernet_ip_tcp_f6_server_cert_split_length; 
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);

	// The version should be set on previous packets not on a change cipher spec
	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(ssl->getTotalHandshakes() == 2);
	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalCertificateRequests() == 0);
}

BOOST_AUTO_TEST_CASE (test14) // Return the issuer from the cert
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f7_ssl_server_cert_done);
        int length = raw_packet_ethernet_ip_tcp_f7_ssl_server_cert_done_length; 
        Packet packet(pkt, length);

	// enable dynamic memory
	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	Flow *flow = ssl->getCurrentFlow();

	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);
        BOOST_CHECK(info->issuer != nullptr);

        std::string cad("Let's Encrypt Authority X3");

        BOOST_CHECK(cad.compare(info->issuer->getName()) == 0);

	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 1);
}

BOOST_AUTO_TEST_CASE (test15) // Return the issuer from the cert from google
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f8_ssl_server_cert_split);
        int length = raw_packet_ethernet_ip_tcp_f8_ssl_server_cert_split_length; 
        Packet packet(pkt, length);

	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	BOOST_CHECK(ssl->getTotalBytes() == 1430);

	Flow *flow = ssl->getCurrentFlow();
	
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);
        BOOST_CHECK(info->issuer != nullptr);

        std::string cad("Google Internet Authority");

        BOOST_CHECK(cad.compare(info->issuer->getName()) == 0);

	BOOST_CHECK(info->getVersion() == TLS1_1_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 0); 

        std::filebuf fb;
        fb.open ("/dev/null",std::ios::out);
        std::ostream outp(&fb);
        flow->serialize(outp);
        flow->showFlowInfo(outp);
        outp << *(info.get());
        fb.close();

        JsonFlow j;
        info->serialize(j);
}

BOOST_AUTO_TEST_CASE (test16) // Return the issuer from the cert 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f9_ssl_server_cert_split);
        int length = raw_packet_ethernet_ip_tcp_f9_ssl_server_cert_split_length; 
        Packet packet(pkt, length);

	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	BOOST_CHECK(ssl->getTotalBytes() == 1460);

	Flow *flow = ssl->getCurrentFlow();
	
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);
        BOOST_CHECK(info->issuer != nullptr);

        std::string cad("Go Daddy Secure Certificate Authority - G2");

        BOOST_CHECK(cad.compare(info->issuer->getName()) == 0);

	BOOST_CHECK(info->getVersion() == TLS1_2_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 0); 
}

BOOST_AUTO_TEST_CASE (test17) // Tor certificate
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_f1_ssl_tor_server_cert_key_done);
        int length = raw_packet_ethernet_ip_tcp_f1_ssl_tor_server_cert_key_done_length; 
        Packet packet(pkt, length);

	ssl->setDynamicAllocatedMemory(true);

	inject(packet);

	BOOST_CHECK(ssl->getTotalBytes() == 929);

	Flow *flow = ssl->getCurrentFlow();
	
	SharedPointer<SSLInfo> info = flow->getSSLInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->host_name == nullptr);
        BOOST_CHECK(info->issuer != nullptr);

        std::string cad("www.o2ihd54volj4icngap.com");

        BOOST_CHECK(cad.compare(info->issuer->getName()) == 0);

	BOOST_CHECK(info->getVersion() == TLS1_VERSION);
        BOOST_CHECK(info->getHeartbeat() == false);

	BOOST_CHECK(ssl->getTotalChangeCipherSpecs() == 0);
	BOOST_CHECK(ssl->getTotalClientHellos() == 0);
	BOOST_CHECK(ssl->getTotalServerHellos() == 1);
	BOOST_CHECK(ssl->getTotalCertificates() == 1);
	BOOST_CHECK(ssl->getTotalServerDones() == 1); 
	BOOST_CHECK(ssl->getTotalRecords() == 4);
}

BOOST_AUTO_TEST_CASE (test18) // Two flows share the same issuer
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_f9_ssl_server_cert_split[54]);
        int length = raw_packet_ethernet_ip_tcp_f9_ssl_server_cert_split_length - 54;
        Packet packet1(pkt, length);
        Packet packet2(pkt, length);

       	auto flow1 = SharedPointer<Flow>(new Flow());
        auto flow2 = SharedPointer<Flow>(new Flow());

        ssl->setDynamicAllocatedMemory(true);

        flow1->packet = const_cast<Packet*>(&packet1);
        flow2->packet = const_cast<Packet*>(&packet2);

        ssl->processFlow(flow1.get());
        ssl->processFlow(flow2.get());

        BOOST_CHECK(ssl->getTotalBytes() == 1460 * 2);

        SharedPointer<SSLInfo> info1 = flow1->getSSLInfo();
        SharedPointer<SSLInfo> info2 = flow2->getSSLInfo();
        BOOST_CHECK(info1 != nullptr);
        BOOST_CHECK(info2 != nullptr);
        BOOST_CHECK(info1->issuer != nullptr);
        BOOST_CHECK(info1->issuer == info2->issuer);

        std::string cad("Go Daddy Secure Certificate Authority - G2");

        BOOST_CHECK(cad.compare(info1->issuer->getName()) == 0);
}

BOOST_AUTO_TEST_SUITE_END()
