/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_dcerpc.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE dcerpctest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(dcerpc_test_suite, StackDCERPCtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet;

	BOOST_CHECK(dcerpc->getTotalBytes() == 0);
	BOOST_CHECK(dcerpc->getTotalPackets() == 0);
	BOOST_CHECK(dcerpc->getTotalValidPackets() == 0);
	BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);
	BOOST_CHECK(dcerpc->processPacket(packet) == true);
	
	CounterMap c = dcerpc->getCounters();
}

BOOST_AUTO_TEST_CASE (test02)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_alter_context_resp_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_alter_context_resp_1_length;
        Packet packet(pkt, length);

	inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

	BOOST_CHECK(dcerpc->getTotalBytes() == 105);
	BOOST_CHECK(dcerpc->getTotalPackets() == 1);
	BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
	BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

	BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes()); 
	BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_ALTER_CONTEXT_RESP); 
}

BOOST_AUTO_TEST_CASE (test03)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_bind_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_bind_1_length;
        Packet packet(pkt, length);

	dcerpc->increaseAllocatedMemory(1);

	inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

	BOOST_CHECK(dcerpc->getTotalBytes() == 160);
	BOOST_CHECK(dcerpc->getTotalPackets() == 1);
	BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
	BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

	BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes()); 
	BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_BIND); // Bind

	SharedPointer<DCERPCInfo> info = flow->getDCERPCInfo();
	BOOST_CHECK(info != nullptr);

}

BOOST_AUTO_TEST_CASE (test04)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_bind_ack_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_bind_ack_1_length;
        Packet packet(pkt, length);

	inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

	BOOST_CHECK(dcerpc->getTotalBytes() == 72);
	BOOST_CHECK(dcerpc->getTotalPackets() == 1);
	BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
	BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

	BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes()); 
	BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_BIND_ACK); // Bind ack
}

BOOST_AUTO_TEST_CASE (test05)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_orphaned_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_orphaned_1_length;
        Packet packet(pkt, length);

	inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

	BOOST_CHECK(dcerpc->getTotalBytes() == 16);
	BOOST_CHECK(dcerpc->getTotalPackets() == 1);
	BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
	BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

	BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes()); 
	BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_ORPHANED);
}

BOOST_AUTO_TEST_CASE (test06)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_alter_context_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_alter_context_1_length;
        Packet packet(pkt, length);

        inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

        BOOST_CHECK(dcerpc->getTotalBytes() == 220);
        BOOST_CHECK(dcerpc->getTotalPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

        BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes()); 
        BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_ALTER_CONTEXT);
}

BOOST_AUTO_TEST_CASE (test07)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_fault_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_fault_1_length;
        Packet packet(pkt, length);

        inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

        BOOST_CHECK(dcerpc->getTotalBytes() == 32);
        BOOST_CHECK(dcerpc->getTotalPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

        BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes());
        BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_FAULT);
}

BOOST_AUTO_TEST_CASE (test08)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_request_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_request_1_length;
        Packet packet(pkt, length);

        inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

        BOOST_CHECK(dcerpc->getTotalBytes() == 168);
        BOOST_CHECK(dcerpc->getTotalPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

        BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes());
        BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_REQUEST);
}

BOOST_AUTO_TEST_CASE (test09)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_response_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_response_1_length;
        Packet packet(pkt, length);

        inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

        BOOST_CHECK(dcerpc->getTotalBytes() == 172);
        BOOST_CHECK(dcerpc->getTotalPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

        BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes());
        BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_RESPONSE);
}

BOOST_AUTO_TEST_CASE (test10)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_auth3_1);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_auth3_1_length;
        Packet packet(pkt, length);

        inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

        BOOST_CHECK(dcerpc->getTotalBytes() == 190);
        BOOST_CHECK(dcerpc->getTotalPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

        BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes());
        BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_AUTH3);
}

BOOST_AUTO_TEST_CASE (test11)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_dcerpc_bind_2);
        int length = raw_packet_ethernet_ip_tcp_dcerpc_bind_2_length;
        Packet packet(pkt, length);

        dcerpc->increaseAllocatedMemory(1);

        inject(packet);

        Flow *flow = dcerpc->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

        BOOST_CHECK(dcerpc->getTotalBytes() == 160);
        BOOST_CHECK(dcerpc->getTotalPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalValidPackets() == 1);
        BOOST_CHECK(dcerpc->getTotalInvalidPackets() == 0);

        BOOST_CHECK(dcerpc->getFragmentLength() == dcerpc->getTotalBytes());
        BOOST_CHECK(dcerpc->getPacketType() == DCERPC_UNIT_BIND); // Bind

        SharedPointer<DCERPCInfo> info = flow->getDCERPCInfo();
        BOOST_CHECK(info != nullptr);

	std::string uuid("e1af8308-5d1f-11c9-91a4-08002b14a0fa");
	BOOST_CHECK(info->uuid != nullptr);

	BOOST_CHECK(uuid.compare(info->uuid->getName()) == 0);

        std::filebuf fb;
        fb.open ("/dev/null",std::ios::out);
        std::ostream outp(&fb);
        flow->serialize(outp);
        flow->showFlowInfo(outp);
        outp << *(info.get());
        fb.close();

        JsonFlow j;
        info->serialize(j);

        dcerpc->releaseCache();
        BOOST_CHECK(flow->getDCERPCInfo() == nullptr);
       
	dcerpc->decreaseAllocatedMemory(1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(ipv6_dcerpc_test_suite, StackIPv6DCERPCtest)

/*
BOOST_AUTO_TEST_CASE (test01)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_ethernet_ip6_udp_rtp2_maker_true_21208);
        int length = raw_ethernet_ip6_udp_rtp2_maker_true_21208_length;
        Packet packet(pkt, length);

	inject(packet);

        Flow *flow = rtp->getCurrentFlow();

        BOOST_CHECK(flow != nullptr);

	BOOST_CHECK(ip6->getTotalBytes() == 40 + 44 + 8);

	BOOST_CHECK(rtp->getTotalBytes() == 44);
	BOOST_CHECK(rtp->getTotalPackets() == 1);
	BOOST_CHECK(rtp->getTotalValidPackets() == 1);
	BOOST_CHECK(rtp->getTotalInvalidPackets() == 0);

	BOOST_CHECK(rtp->getVersion() == 2); 
	BOOST_CHECK(rtp->getPayloadType() == 102); // AMR mode
	BOOST_CHECK(rtp->getPadding() == false); 
}

*/

BOOST_AUTO_TEST_SUITE_END()
