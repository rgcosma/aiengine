/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_DCERPC_DCERPCPROTOCOL_H_
#define SRC_PROTOCOLS_DCERPC_DCERPCPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_LIBLOG4CXX
#include "log4cxx/logger.h"
#endif
#include "Protocol.h"
#include <arpa/inet.h>
#include "flow/FlowManager.h"
#include "CacheManager.h"
#include "Cache.h"

namespace aiengine {

#define DCERPC_VERSION 5 

typedef struct __attribute__((packed)) {
	uint8_t version;	/* protocol version */
	uint8_t version_minor; 	/* protocol version */
    	uint8_t packet_type;    /* packet type */
    	uint8_t packet_flags;   /* packet flags */
	uint32_t data_repr;     /* data representation */
	uint16_t frag_length;   /* fragment length */
	uint16_t auth_lenght;   /* auth length */
	uint32_t callid;
	u_char data[0];
} dcerpc_hdr;

typedef struct __attribute__((packed)) {
        uint16_t context_id;	/* context id */
	uint16_t items;		/* items */
	u_char uuid[16];	/* uuid */
	uint16_t intface_ver; 	/* interface version mayor */
	uint16_t intface_minor; /* interface version minor */
        u_char data[0];
} dcerpc_context_item_hdr;

//pubs.opengroup.org/onlinepubs/9629399/chap12.htm
enum dcerpc_unit_types {
        DCERPC_UNIT_REQUEST 		= 0, 
        DCERPC_UNIT_PING		= 1, 
        DCERPC_UNIT_RESPONSE		= 2, 
        DCERPC_UNIT_FAULT 		= 3,
        DCERPC_UNIT_WORKING		= 4, 
        DCERPC_UNIT_NOCALL		= 5, 
        DCERPC_UNIT_REJECT		= 6,
        DCERPC_UNIT_ACK			= 7,
        DCERPC_UNIT_CL_CANCEL,
        DCERPC_UNIT_FACK,
        DCERPC_UNIT_CANCEL_ACK,
        DCERPC_UNIT_BIND		= 11,
        DCERPC_UNIT_BIND_ACK,
        DCERPC_UNIT_BIND_NAK,
        DCERPC_UNIT_ALTER_CONTEXT	= 14,
        DCERPC_UNIT_ALTER_CONTEXT_RESP	= 15,
        DCERPC_UNIT_AUTH3		= 16,
        DCERPC_UNIT_SHUTDOWN 		= 17,
        DCERPC_UNIT_CO_CANCEL		= 18,
        DCERPC_UNIT_ORPHANED		= 19
};

class DCERPCProtocol: public Protocol {
public:
    	explicit DCERPCProtocol();
    	virtual ~DCERPCProtocol();

	static const uint16_t id = 0;	
	static constexpr int header_size = sizeof(dcerpc_hdr);

	int getHeaderSize() const { return header_size;}

        void processFlow(Flow *flow);
        bool processPacket(Packet& packet) { return true; } 

	void statistics(std::basic_ostream<char>& out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char>& out, int level);

	void releaseCache();

	void setHeader(unsigned char *raw_packet) { 

		dcerpc_header_ = reinterpret_cast <dcerpc_hdr*> (raw_packet);
	}

	// Condition for say that a packet is dcerpc
	bool dcerpcChecker(Packet &packet); 

	// Protocol specific methods
	uint8_t getPacketType() const { return dcerpc_header_->packet_type; }
	uint16_t getFragmentLength() const { return dcerpc_header_->frag_length; }
	
	int64_t getCurrentUseMemory() const; 
	int64_t getAllocatedMemory() const;
	int64_t getTotalAllocatedMemory() const;

	void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	void increaseAllocatedMemory(int value); 
	void decreaseAllocatedMemory(int value); 

        void setDynamicAllocatedMemory(bool value); 
        bool isDynamicAllocatedMemory() const; 

	CounterMap getCounters() const; 

        void setCacheManager(SharedPointer<CacheManager> cmng) { cache_mng_ = cmng; }

	Flow *getCurrentFlow() const { return current_flow_; }
private:
	void update_unit_type(uint8_t type);
	void process_bind_message(DCERPCInfo *info, unsigned char *payload, int length);

	void attach_uuid(DCERPCInfo *info, boost::string_ref &uuid);
	int64_t compute_memory_used_by_maps() const; 
	int32_t release_dcerpc_info(DCERPCInfo *info); 

	dcerpc_hdr *dcerpc_header_;

	// Some statistics 
	int32_t total_requests_;
	int32_t total_pings_;
	int32_t total_responses_;
	int32_t total_faults_;
	int32_t total_workings_;
	int32_t total_nocalls_;
	int32_t total_rejects_;
	int32_t total_acks_;
	int32_t total_cl_cancels_;
	int32_t total_facks_;
	int32_t total_cancel_acks_;
	int32_t total_binds_;
	int32_t total_bind_acks_;
	int32_t total_bind_naks_;
	int32_t total_alter_contexts_;
	int32_t total_alter_context_resps_;
	int32_t total_auth3s_;
	int32_t total_shutdonws_;
	int32_t total_co_cancels_;
	int32_t total_orphaneds_;
	int32_t total_others_;

        Flow *current_flow_;

        Cache<DCERPCInfo>::CachePtr info_cache_;
        Cache<StringCache>::CachePtr uuid_cache_;

        GenericMapType uuid_map_;

        FlowManagerPtrWeak flow_mng_;
        SharedPointer<CacheManager> cache_mng_;
#ifdef HAVE_LIBLOG4CXX
        static log4cxx::LoggerPtr logger;
#endif
};

typedef std::shared_ptr<DCERPCProtocol> DCERPCProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_DCERPC_DCERPCPROTOCOL_H_
