/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_smb.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE smbtest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(smb_test_suite, StackSMBtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet;

	BOOST_CHECK(smb->getTotalBytes() == 0);
	BOOST_CHECK(smb->getTotalPackets() == 0);
	BOOST_CHECK(smb->getTotalValidPackets() == 0);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);
	BOOST_CHECK(smb->processPacket(packet) == true);
	
	CounterMap c = smb->getCounters();

	smb->decreaseAllocatedMemory(10);
}

BOOST_AUTO_TEST_CASE (test02)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_over_port80);
        int length = raw_packet_ethernet_ip_tcp_smbv1_over_port80_length;
        Packet packet(pkt, length);

	inject(packet);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 168);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB_CMD_NEGO_PROTO);

	CounterMap c = smb->getCounters();

	// Force a release
	releaseFlow(flow);
}

BOOST_AUTO_TEST_CASE (test03)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_negotiate_protocol_request);
        int length = raw_packet_ethernet_ip_tcp_smbv1_negotiate_protocol_request_length;
        Packet packet(pkt, length);

	inject(packet);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 194);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);
	
	BOOST_CHECK(info->getCommand() == SMB_CMD_NEGO_PROTO);

}

BOOST_AUTO_TEST_CASE (test04)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_close_file);
        int length = raw_packet_ethernet_ip_tcp_smbv1_close_file_length;
        Packet packet(pkt, length);

	inject(packet);
        
	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 45);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);
	
	BOOST_CHECK(info->getCommand() == SMB_CMD_CLOSE_FILE);
}

BOOST_AUTO_TEST_CASE (test05)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_get_attributes_file);
        int length = raw_packet_ethernet_ip_tcp_smbv1_get_attributes_file_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 96);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);
	
	BOOST_CHECK(info->getCommand() == SMB_CMD_GET_FILE_ATTR);
}

BOOST_AUTO_TEST_CASE (test06)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv2_create_file);
        int length = raw_packet_ethernet_ip_tcp_smbv2_create_file_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 348);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_CREATE_FILE);
}

BOOST_AUTO_TEST_CASE (test07)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_tree_connect);
        int length = raw_packet_ethernet_ip_tcp_smbv1_tree_connect_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 92);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB_CMD_TREE_CONNECT);
}

BOOST_AUTO_TEST_CASE (test08)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv2_tree_connect);
        int length = raw_packet_ethernet_ip_tcp_smbv2_tree_connect_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 108);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_TREE_CONNECT);
}

BOOST_AUTO_TEST_CASE (test09)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv2_close_file);
        int length = raw_packet_ethernet_ip_tcp_smbv2_close_file_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 92);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_CLOSE_FILE);
}

BOOST_AUTO_TEST_CASE (test10)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv2_create_file_putty_exe);
        int length = raw_packet_ethernet_ip_tcp_smbv2_create_file_putty_exe_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 324);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_CREATE_FILE);
        BOOST_CHECK(info->filename != nullptr);

	std::string filename("putty.exe");
	BOOST_CHECK(filename.compare(info->filename->getName()) == 0);

        std::filebuf fb;
        fb.open ("/dev/null",std::ios::out);
        std::ostream outp(&fb);
        flow->serialize(outp);
        flow->showFlowInfo(outp);
        outp << *(info.get());
        fb.close();

        JsonFlow j;
        info->serialize(j);
	
	smb->releaseCache();
	BOOST_CHECK(flow->getSMBInfo() == nullptr);
}

BOOST_AUTO_TEST_CASE (test11)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv2_get_file_info);
        int length = raw_packet_ethernet_ip_tcp_smbv2_get_file_info_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 109);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_GET_INFO);
}

BOOST_AUTO_TEST_CASE (test12)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_trans);
        int length = raw_packet_ethernet_ip_tcp_smbv1_trans_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 182);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB_CMD_TRANS2);
}

BOOST_AUTO_TEST_CASE (test13)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_read);
        int length = raw_packet_ethernet_ip_tcp_smbv1_read_length;
        Packet packet(pkt, length);

	inject(packet);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<NetbiosInfo> ninfo = flow->getNetbiosInfo();
        BOOST_CHECK(ninfo == nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 63);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB_CMD_READ);
}

BOOST_AUTO_TEST_CASE (test14)
{
	unsigned char *pkt1 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_flow1_smbv2_read_request);
        int length1 = raw_packet_ethernet_ip_tcp_flow1_smbv2_read_request_length;
        Packet packet1(pkt1, length1);
	unsigned char *pkt2 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_flow1_smbv2_read_respond);
        int length2 = raw_packet_ethernet_ip_tcp_flow1_smbv2_read_respond_length;
        Packet packet2(pkt2, length2);

	inject(packet1);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 117);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_READ_FILE);
	
	inject(packet2);

	flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 117 + 297);
	BOOST_CHECK(smb->getTotalPackets() == 2);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_READ_FILE);
}

BOOST_AUTO_TEST_CASE (test15)
{
	unsigned char *pkt1 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_flow1_smbv2_write_request);
        int length1 = raw_packet_ethernet_ip_tcp_flow1_smbv2_write_request_length;
        Packet packet1(pkt1, length1);
	unsigned char *pkt2 = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_flow1_smbv2_write_respond);
        int length2 = raw_packet_ethernet_ip_tcp_flow1_smbv2_write_respond_length;
        Packet packet2(pkt2, length2);

	inject(packet1);

	Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 2896);
	BOOST_CHECK(smb->getTotalPackets() == 1);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_WRITE_FILE);
	
	inject(packet2);

	flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
	info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

	BOOST_CHECK(smb->getTotalBytes() == 2896 + 84);
	BOOST_CHECK(smb->getTotalPackets() == 2);
	BOOST_CHECK(smb->getTotalValidPackets() == 1);
	BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

	BOOST_CHECK(info->getCommand() == SMB2_CMD_WRITE_FILE);
}

BOOST_AUTO_TEST_CASE (test16) // malformed packet
{
	unsigned char modbuf[raw_packet_ethernet_ip_tcp_smbv1_read_length]; 
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_read);
	std::memcpy(&modbuf, pkt, raw_packet_ethernet_ip_tcp_smbv1_read_length);
        int length = raw_packet_ethernet_ip_tcp_smbv1_read_length;

	// modify the version with other byte
	modbuf[14 + 20 + 32 + 4] = 0xaa;

        Packet packet(&modbuf[0], length);

        inject(packet);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

        BOOST_CHECK(smb->getTotalBytes() == 63);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        PacketAnomalyType pa = flow->getPacketAnomaly();
        BOOST_CHECK(pa == PacketAnomalyType::SMB_BOGUS_HEADER);
        BOOST_CHECK(smb->getTotalEvents() == 1);

	// No command setup
        BOOST_CHECK(info->getCommand() == 0);
}

BOOST_AUTO_TEST_CASE (test17) // malformed packet
{
	auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_smbv1_read[54]);
        Packet packet(pkt, 7);

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);

        smb->processFlow(flow.get());

        BOOST_CHECK(smb->getTotalBytes() == 7);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 0);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info == nullptr);

        PacketAnomalyType pa = flow->getPacketAnomaly();
        BOOST_CHECK(pa == PacketAnomalyType::SMB_BOGUS_HEADER);
        BOOST_CHECK(smb->getTotalEvents() == 1);
}

BOOST_AUTO_TEST_CASE (test18) // two flows managing the same file
{
        auto flow1 = SharedPointer<Flow>(new Flow());
        auto flow2 = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_tcp_smbv2_create_file_putty_exe[54]);
        int length = raw_packet_ethernet_ip_tcp_smbv2_create_file_putty_exe_length;
        Packet packet(pkt, length - 54);

        flow1->setFlowDirection(FlowDirection::FORWARD);
        flow1->packet = const_cast<Packet*>(&packet);
        flow2->setFlowDirection(FlowDirection::FORWARD);
        flow2->packet = const_cast<Packet*>(&packet);

        smb->processFlow(flow1.get());
        smb->processFlow(flow2.get());

        SharedPointer<SMBInfo> info1 = flow1->getSMBInfo();
        BOOST_CHECK(info1 != nullptr);
        SharedPointer<SMBInfo> info2 = flow2->getSMBInfo();
        BOOST_CHECK(info2 != nullptr);
        BOOST_CHECK(info1->getCommand() == SMB2_CMD_CREATE_FILE);
        BOOST_CHECK(info2->getCommand() == SMB2_CMD_CREATE_FILE);
        BOOST_CHECK(info1->filename != nullptr);
        BOOST_CHECK(info2->filename != nullptr);
        BOOST_CHECK(info2->filename == info2->filename);
}

BOOST_AUTO_TEST_CASE (test19)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_session_setup);
        int length = raw_packet_ethernet_ip_tcp_smbv1_session_setup_length;
        Packet packet(pkt, length);

        inject(packet);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);

        BOOST_CHECK(smb->getTotalBytes() == 164);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        BOOST_CHECK(info->getCommand() == SMB_CMD_SESSION_SETUP);
}

BOOST_AUTO_TEST_CASE (test20) // memory fail
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_session_setup);
        int length = raw_packet_ethernet_ip_tcp_smbv1_session_setup_length;
        Packet packet(pkt, length);

	smb->decreaseAllocatedMemory(10);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 164);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info == nullptr);
}

BOOST_AUTO_TEST_CASE (test21) 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_tree_disconnect);
        int length = raw_packet_ethernet_ip_tcp_smbv1_tree_disconnect_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 39);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_TREE_DISC);
}

BOOST_AUTO_TEST_CASE (test22) 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_logoff_and_request);
        int length = raw_packet_ethernet_ip_tcp_smbv1_logoff_and_request_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 43);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_LOGOFF);
}

BOOST_AUTO_TEST_CASE (test23) 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_nt_create);
        int length = raw_packet_ethernet_ip_tcp_smbv1_nt_create_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 104);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_NT_CREATE);
}

BOOST_AUTO_TEST_CASE (test24) // delete file
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_delete_file);
        int length = raw_packet_ethernet_ip_tcp_smbv1_delete_file_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 56);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_DELETE_FILE);
}

BOOST_AUTO_TEST_CASE (test25) // open andx 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_open_andx);
        int length = raw_packet_ethernet_ip_tcp_smbv1_open_andx_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 83);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_OPEN_ANDX);

        BOOST_CHECK(info->filename != nullptr);

	std::string filename("\\uKKUQIEr.exe");
	BOOST_CHECK(filename.compare(info->filename->getName()) == 0);
}

BOOST_AUTO_TEST_CASE (test26) // rename file
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_rename_file);
        int length = raw_packet_ethernet_ip_tcp_smbv1_rename_file_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 134);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_RENAME_FILE);
}

BOOST_AUTO_TEST_CASE (test27) // write andx message 
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_write_andx);
        int length = raw_packet_ethernet_ip_tcp_smbv1_write_andx_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 51);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_WRITE_ANDX);
}

BOOST_AUTO_TEST_CASE (test28) // create message
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_create_file);
        int length = raw_packet_ethernet_ip_tcp_smbv1_create_file_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 102);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_CREATE_FILE);
        BOOST_CHECK(info->filename != nullptr);

        std::string filename("\\rawopen\\torture_create.txt");
        BOOST_CHECK(filename.compare(info->filename->getName()) == 0);
}

BOOST_AUTO_TEST_CASE (test29) // Flush
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_flush_file);
        int length = raw_packet_ethernet_ip_tcp_smbv1_flush_file_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 41);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_FLUSH_FILES);
}

BOOST_AUTO_TEST_CASE (test30) // set attributes
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_set_attributes);
        int length = raw_packet_ethernet_ip_tcp_smbv1_set_attributes_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 126);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_SET_FILE_ATTR);
}

BOOST_AUTO_TEST_CASE (test31) // create directory
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_create_directory);
        int length = raw_packet_ethernet_ip_tcp_smbv1_create_directory_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 88);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_CREATE_DIR);
        BOOST_CHECK(info->filename != nullptr);

        std::string filename("\\rawchkpath\\nt\\V S\\VB98");
        BOOST_CHECK(filename.compare(info->filename->getName()) == 0);
}

BOOST_AUTO_TEST_CASE (test32) // delete directory
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_delete_directory);
        int length = raw_packet_ethernet_ip_tcp_smbv1_delete_directory_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 96);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == SMB_CMD_DELETE_DIR);
}

BOOST_AUTO_TEST_CASE (test33) // process exist
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv1_process_exit);
        int length = raw_packet_ethernet_ip_tcp_smbv1_process_exit_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 39);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == 17);
}

BOOST_AUTO_TEST_CASE (test34) // ioctl response error
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smbv2_ioctl_response_error);
        int length = raw_packet_ethernet_ip_tcp_smbv2_ioctl_response_error_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smb->getTotalBytes() == 77);
        BOOST_CHECK(smb->getTotalPackets() == 1);
        BOOST_CHECK(smb->getTotalValidPackets() == 1);
        BOOST_CHECK(smb->getTotalInvalidPackets() == 0);

        Flow *flow = smb->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<SMBInfo> info = flow->getSMBInfo();
        BOOST_CHECK(info != nullptr);
        BOOST_CHECK(info->getCommand() == 11);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(ipv6_smb_test_suite, StackIPv6SMBtest)

BOOST_AUTO_TEST_CASE (test01)
{
/*
 * TODO
 */
}

BOOST_AUTO_TEST_SUITE_END()
