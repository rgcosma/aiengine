/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_imap.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE_TEST
#define BOOST_TEST_MODULE imaptest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(test_suite_imap, StackIMAPtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet;

        BOOST_CHECK(imap->getTotalPackets() == 0);
        BOOST_CHECK(imap->getTotalValidPackets() == 0);
        BOOST_CHECK(imap->getTotalInvalidPackets() == 0);
        BOOST_CHECK(imap->getTotalBytes() == 0);
	BOOST_CHECK(imap->processPacket(packet) == true);
}

BOOST_AUTO_TEST_CASE (test02)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_imap_server_banner);
        int length = raw_packet_ethernet_ip_tcp_imap_server_banner_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(imap->getTotalPackets() == 1);
        BOOST_CHECK(imap->getTotalValidPackets() == 1);
        BOOST_CHECK(imap->getTotalBytes() == 42);

        std::string cad("* OK IMAP4Rev1 Server Version 4.9.04.012");
        std::ostringstream h;

        h << imap->getPayload();
	
        BOOST_CHECK(cad.compare(0,cad.size(),h.str(),0,cad.size()) == 0);
}

BOOST_AUTO_TEST_CASE (test03)
{
        char *header =  "C00000 CAPABILITY\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        imap->processFlow(flow.get());

        BOOST_CHECK(imap->getTotalBytes() == 19);

        std::string cad("C00000 CAPABILITY");
        std::ostringstream h;

        h << imap->getPayload();
        BOOST_CHECK(cad.compare(0,cad.length(),h.str(),0,cad.length()) == 0);
	BOOST_CHECK(imap->getTotalEvents() == 0);
}

BOOST_AUTO_TEST_CASE (test04)
{
        char *header =  "00001 LOGIN pepe mypassword\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        imap->processFlow(flow.get());

        BOOST_CHECK(imap->getTotalBytes() == length);

        //BOOST_CHECK(cad.compare(0,cad.length(),h.str(),0,cad.length()) == 0);
}

BOOST_AUTO_TEST_CASE (test05)
{
        char *header =  "00001 LOGIN pepe@meneame.net mypassword\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt, length);

	imap->increaseAllocatedMemory(1);
        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        imap->processFlow(flow.get());

        BOOST_CHECK(imap->getTotalBytes() == length);

	auto info = flow->getIMAPInfo();
	BOOST_CHECK(info != nullptr);
	BOOST_CHECK(info->user_name != nullptr);
}

BOOST_AUTO_TEST_CASE (test06)
{
        char *header =  "00001 LOGIN pepe@meneameandsomebigggbuerferexc.netmypassword";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        imap->processFlow(flow.get());

        BOOST_CHECK(imap->getTotalBytes() == length);
	BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::IMAP_BOGUS_HEADER);
	BOOST_CHECK(imap->getTotalEvents() == 1);

	CounterMap c = imap->getCounters();
}

BOOST_AUTO_TEST_CASE (test07)
{
	auto dm = SharedPointer<DomainNameManager>(new DomainNameManager());
	auto d = SharedPointer<DomainName>(new DomainName("bu","meneame.net"));

	dm->addDomainName(d);

        char *header1 =  "00001 LOGIN pepe@meneame.net mypassword\r\n";
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (header1);
        int length1 = strlen(header1);
        Packet packet1(pkt1, length1);

	imap->increaseAllocatedMemory(1);
	imap->setDomainNameBanManager(dm);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet1);
        imap->processFlow(flow.get());

	// Not interested on meneame users
	BOOST_CHECK(d->getMatchs() == 1);
	auto info = flow->getIMAPInfo();
	BOOST_CHECK(info != nullptr);
	BOOST_CHECK(info->user_name == nullptr);
	BOOST_CHECK(info->isBanned() == true);
        
	char *header2 =  "00001 OK LOGIN completed\r\n";
        unsigned char *pkt2 = reinterpret_cast <unsigned char*> (header2);
        int length2 = strlen(header2);
        Packet packet2(pkt2, length2);
        
	flow->setFlowDirection(FlowDirection::BACKWARD);
        flow->packet = const_cast<Packet*>(&packet2);
        imap->processFlow(flow.get());

	BOOST_CHECK(d->getMatchs() == 1);
	BOOST_CHECK(info != nullptr);
	BOOST_CHECK(info->user_name == nullptr);
	BOOST_CHECK(info->isBanned() == true);
}

BOOST_AUTO_TEST_CASE (test08)
{
        auto dm = SharedPointer<DomainNameManager>(new DomainNameManager());
        auto d = SharedPointer<DomainName>(new DomainName("bogus domain","snowden.ru"));

        dm->addDomainName(d);

        char *header =  "00001 LOGIN letmein@snowden.ru mypassword\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt, length);

        imap->increaseAllocatedMemory(1);
        imap->setDomainNameManager(dm);

        auto flow1 = SharedPointer<Flow>(new Flow());
        auto flow2 = SharedPointer<Flow>(new Flow());

        flow1->setFlowDirection(FlowDirection::FORWARD);
        flow1->packet = const_cast<Packet*>(&packet);
        flow2->setFlowDirection(FlowDirection::FORWARD);
        flow2->packet = const_cast<Packet*>(&packet);

        imap->processFlow(flow1.get());
        imap->processFlow(flow2.get());

        auto info1 = flow1->getIMAPInfo();
        auto info2 = flow2->getIMAPInfo();
        BOOST_CHECK(info1 != nullptr);
        BOOST_CHECK(info2 != nullptr);
        BOOST_CHECK(info1->user_name != nullptr);
        BOOST_CHECK(info1->isBanned() == false);
        BOOST_CHECK(info2->user_name != nullptr);
        BOOST_CHECK(info2->isBanned() == false);

        BOOST_CHECK(info1->user_name == info2->user_name);

        BOOST_CHECK(d->getMatchs() == 2);
}

BOOST_AUTO_TEST_CASE (test09) // memory failure
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_imap_server_banner);
        int length = raw_packet_ethernet_ip_tcp_imap_server_banner_length;
        Packet packet(pkt, length);

	imap->decreaseAllocatedMemory(100);

	inject(packet);

        BOOST_CHECK(imap->getTotalPackets() == 1);
        BOOST_CHECK(imap->getTotalValidPackets() == 1);
        BOOST_CHECK(imap->getTotalBytes() == 42);

	Flow *flow = imap->getCurrentFlow();
	BOOST_CHECK(flow != nullptr);
	BOOST_CHECK(flow->getIMAPInfo() == nullptr);
}

BOOST_AUTO_TEST_SUITE_END()
