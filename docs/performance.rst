In this section we are going to explore and compare the different performance values such as CPU and memory comsumption
with other engines such as tshark, snort, suricata and nDPI.

The main tools used for evaluate the performance is perf(https://linux.die.net/man/1/perf-stat).

+----------+---------+
| Tool     | Version |
+==========+=========+
| Snort    | 2.9.9.0 |
+----------+---------+
| Tshark   |   2.0.2 |
+----------+---------+
| Suricata |   3.2.1 |
+----------+---------+
| nDPI     |   2.1.0 |
+----------+---------+
| AIEngine |   1.8.1 |
+----------+---------+

The machine is a 8 CPUS Intel(R) Core(TM) i7-6820HQ CPU @ 2.70GHz with 16 GB memory. 

The first pcap file use is from (http://www.unb.ca/cic/research/datasets/index.html) is aproximately 17GB size with the mayority of traffic HTTP.
The pcap file used for these tests contains a distribution of traffic shown below

+-----------------+------------+---------+-------------+
| Network Protocol| Percentage | Bytes   | Packets     |
+=================+============+=========+=============+
| IPv4            |        97% | 12154MB |    17292813 |
+-----------------+------------+---------+-------------+
| TCP             |        95% | 11821MB |    17029774 | 
+-----------------+------------+---------+-------------+
| HTTP            |        88% | 11001MB |     9237421 | 
+-----------------+------------+---------+-------------+
| SSL             |         1% |   205MB |      223309 |
+-----------------+------------+---------+-------------+

The second pcap file used is from (https://download.netresec.com/pcap/ists-12/2015-03-07/). We downloaded the first 55 files and generate a pcap file about 8GB.
The pcap file used for these tests contains a distribution of traffic shown below

+-----------------+------------+---------+-------------+
| Network Protocol| Percentage | Bytes   | Packets     |
+=================+============+=========+=============+
| IPv4            |        97% |  7604MB |    13512877 |
+-----------------+------------+---------+-------------+
| TCP             |        88% |  6960MB |    12261324 |
+-----------------+------------+---------+-------------+
| UDP             |         4% |   374MB |      928563 |
+-----------------+------------+---------+-------------+
| HTTP            |        27% |  2160MB |     1763905 |
+-----------------+------------+---------+-------------+
| SSL             |        38% |  3046MB |     2508241 |
+-----------------+------------+---------+-------------+

The thrird pcap file used is from (https://www.unsw.adfa.edu.au/australian-centre-for-cyber-security/cybersecurity/ADFA-NB15-Datasets/). We downloaded 10 samples and generate
a pcap file of 20GB. The traffic distribution is shown bellow.

+-----------------+------------+---------+-------------+
| Network Protocol| Percentage | Bytes   | Packets     |
+=================+============+=========+=============+
| IPv4            |        97% | 17997MB |    35214962 |
+-----------------+------------+---------+-------------+
| TCP             |        93% | 17282MB |    34628721 |
+-----------------+------------+---------+-------------+
| HTTP            |        25% |  4675MB |     3639612 |
+-----------------+------------+---------+-------------+
| SMTP            |         5% |   952MB |     1126442 |
+-----------------+------------+---------+-------------+
| IMAP            |         1% |   192MB |     1196752 |
+-----------------+------------+---------+-------------+


Be aware that the results depends on the type of traffic of the network.

Test I
......

In this section we are going to perform the first pcap (http://www.unb.ca/cic/research/datasets/index.html)


Test I processing traffic
~~~~~~~~~~~~~~~~~~~~~~~~~

In this section we explore how fast are the engines just processing the traffic without any rules or any logic on them.

Snort
*****

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

      64269.015098      task-clock (msec)         #    0.981 CPUs utilized          
             1,760      context-switches          #    0.027 K/sec                  
                36      cpu-migrations            #    0.001 K/sec                  
            44,841      page-faults               #    0.698 K/sec                  
   204,394,163,771      cycles                    #    3.180 GHz                    
   375,256,677,520      instructions              #    1.84  insns per cycle        
    98,031,161,725      branches                  # 1525.325 M/sec                  
       565,404,035      branch-misses             #    0.58% of all branches        

      65.487290231 seconds time elapsed

Tshark
******

.. code:: bash

   Performance counter stats for 'tshark -q -z conv,tcp -r /pcaps/iscx/testbed-17jun.pcap':

     112070.498904      task-clock (msec)         #    0.909 CPUs utilized          
            11,390      context-switches          #    0.102 K/sec                  
               261      cpu-migrations            #    0.002 K/sec                  
         2,172,942      page-faults               #    0.019 M/sec                  
   310,196,020,123      cycles                    #    2.768 GHz                    
   449,687,949,322      instructions              #    1.45  insns per cycle        
    99,620,662,743      branches                  #  888.911 M/sec                  
       729,598,416      branch-misses             #    0.73% of all branches        

     123.265736897 seconds time elapsed

Suricata
********

With 9 packet processing threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     100446.349460      task-clock (msec)         #    3.963 CPUs utilized          
         2,264,381      context-switches          #    0.023 M/sec                  
           220,905      cpu-migrations            #    0.002 M/sec                  
           108,722      page-faults               #    0.001 M/sec                  
   274,824,170,581      cycles                    #    2.736 GHz                    
   249,152,605,118      instructions              #    0.91  insns per cycle        
    56,052,176,697      branches                  #  558.031 M/sec                  
       538,776,158      branch-misses             #    0.96% of all branches        

      25.345742192 seconds time elapsed

With one packet processing thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

      94797.134432      task-clock (msec)         #    1.989 CPUs utilized          
           124,424      context-switches          #    0.001 M/sec                  
             1,158      cpu-migrations            #    0.012 K/sec                  
            71,535      page-faults               #    0.755 K/sec                  
   261,166,110,590      cycles                    #    2.755 GHz                    
   306,188,504,447      instructions              #    1.17  insns per cycle        
    72,333,018,827      branches                  #  763.030 M/sec                  
       468,673,879      branch-misses             #    0.65% of all branches        

      47.668130400 seconds time elapsed

nDPI
****

.. code:: bash

   Performance counter stats for './ndpiReader -i /pcaps/iscx/testbed-17jun.pcap':

      20134.419533      task-clock (msec)         #    0.758 CPUs utilized          
            78,990      context-switches          #    0.004 M/sec                  
               104      cpu-migrations            #    0.005 K/sec                  
            44,408      page-faults               #    0.002 M/sec                  
    55,566,151,984      cycles                    #    2.760 GHz                    
    62,980,097,786      instructions              #    1.13  insns per cycle        
    15,048,874,292      branches                  #  747.420 M/sec                  
       281,671,995      branch-misses             #    1.87% of all branches        

      26.559667812 seconds time elapsed

AIengine
********

.. code:: bash

    Performance counter stats for './aiengine -i /pcaps/iscx/testbed-17jun.pcap -o':

      18039.084521      task-clock (msec)         #    0.704 CPUs utilized          
            91,186      context-switches          #    0.005 M/sec                  
               476      cpu-migrations            #    0.026 K/sec                  
            17,430      page-faults               #    0.966 K/sec                  
    50,049,479,531      cycles                    #    2.775 GHz                    
    67,835,601,309      instructions              #    1.36  insns per cycle        
    14,522,520,700      branches                  #  805.059 M/sec                  
       166,645,083      branch-misses             #    1.15% of all branches        

      25.610078128 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 204,394M |     375,256M |      65 |
+-------------+----------+--------------+---------+
| Tshark      | 310,196M |      99,620M |     123 |
+-------------+----------+--------------+---------+
| Suricata(9) | 274,824M |     249,152M |      25 |
+-------------+----------+--------------+---------+
| Suricata(1) | 261,166M |     306,188M |      47 |
+-------------+----------+--------------+---------+
| nDPI        |  55,566M |      62,980M |      26 |
+-------------+----------+--------------+---------+
| AIEngine    |  50,049M |      67,835M |      25 |
+-------------+----------+--------------+---------+

Tests I with rules
~~~~~~~~~~~~~~~~~~

On this section we evalute simple rules in order to compare the different systems.

The rule that we are going to use is quite simple, it consists on find the string "cmd.exe" on the payload of all the TCP traffic.

Snort
*****

.. code:: bash

   alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1)

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

     271091.019789      task-clock (msec)         #    0.994 CPUs utilized          
             3,213      context-switches          #    0.012 K/sec                  
                80      cpu-migrations            #    0.000 K/sec                  
            65,124      page-faults               #    0.240 K/sec                  
   731,608,435,272      cycles                    #    2.699 GHz                    
 1,033,203,748,622      instructions              #    1.41  insns per cycle        
   193,558,431,134      branches                  #  713.998 M/sec                  
       655,588,320      branch-misses             #    0.34% of all branches        

     272.704320602 seconds time elapsed

Suricata
********

.. code:: bash

   alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

With 9 packet processing threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     147104.764348      task-clock (msec)         #    4.864 CPUs utilized          
         1,380,685      context-switches          #    0.009 M/sec                  
            49,927      cpu-migrations            #    0.339 K/sec                  
           388,670      page-faults               #    0.003 M/sec                  
   404,341,193,048      cycles                    #    2.749 GHz                    
   426,566,148,876      instructions              #    1.05  insns per cycle        
    80,421,852,312      branches                  #  546.698 M/sec                  
       624,570,278      branch-misses             #    0.78% of all branches        

      30.242149664 seconds time elapsed

With one packet processing thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     158579.888281      task-clock (msec)         #    1.976 CPUs utilized          
            97,030      context-switches          #    0.612 K/sec                  
             1,143      cpu-migrations            #    0.007 K/sec                  
            52,539      page-faults               #    0.331 K/sec                  
   442,028,848,482      cycles                    #    2.787 GHz                    
   591,840,610,271      instructions              #    1.34  insns per cycle        
   125,011,110,377      branches                  #  788.316 M/sec                  
       493,436,768      branch-misses             #    0.39% of all branches        

      80.250462424 seconds time elapsed

AIEngine
********

Rule: "cmd.exe"

.. code:: bash

   Performance counter stats for './aiengine -i /pcaps/iscx/testbed-17jun.pcap -R -r cmd.exe -m -c tcp':

      25815.755873      task-clock (msec)         #    0.945 CPUs utilized          
            38,008      context-switches          #    0.001 M/sec                  
               109      cpu-migrations            #    0.004 K/sec                  
             2,587      page-faults               #    0.100 K/sec                  
    79,739,426,930      cycles                    #    3.089 GHz                    
   170,908,065,937      instructions              #    2.14  insns per cycle        
    48,678,620,025      branches                  # 1885.617 M/sec                  
       438,618,038      branch-misses             #    0.90% of all branches        

      27.323182412 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 731,608M |   1,033,203M |     272 |
+-------------+----------+--------------+---------+
| Suricata(9) | 404,341M |     426,566M |      30 |
+-------------+----------+--------------+---------+
| Suricata(1) | 442,028M |     591,840M |      80 |
+-------------+----------+--------------+---------+
| AIEngine    |  79,739M |     170,908M |      27 |
+-------------+----------+--------------+---------+

Snort
*****

A simliar rules as before but just trying to help a bit to Snort.

.. code:: bash

   alert tcp any any -> any 80 (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

      70456.213488      task-clock (msec)         #    0.984 CPUs utilized          
             5,901      context-switches          #    0.084 K/sec                  
                63      cpu-migrations            #    0.001 K/sec                  
            79,927      page-faults               #    0.001 M/sec                  
   214,846,354,228      cycles                    #    3.049 GHz                    
   385,107,871,838      instructions              #    1.79  insns per cycle        
   100,011,250,526      branches                  # 1419.481 M/sec                  
       579,460,528      branch-misses             #    0.58% of all branches        

      71.582493144 seconds time elapsed

Suricata
********

Change the rule just for HTTP traffic

.. code:: bash

   alert http any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     140314.604419      task-clock (msec)         #    5.007 CPUs utilized          
         1,326,047      context-switches          #    0.009 M/sec                  
            81,882      cpu-migrations            #    0.584 K/sec                  
           287,767      page-faults               #    0.002 M/sec                  
   385,297,597,444      cycles                    #    2.746 GHz                    
   427,295,175,085      instructions              #    1.11  insns per cycle        
    80,682,776,679      branches                  #  575.013 M/sec                  
       570,289,598      branch-misses             #    0.71% of all branches        

      28.023789653 seconds time elapsed

With one processing packet thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     148652.663600      task-clock (msec)         #    1.974 CPUs utilized          
            96,622      context-switches          #    0.650 K/sec                  
               637      cpu-migrations            #    0.004 K/sec                  
            53,167      page-faults               #    0.358 K/sec                  
   426,698,526,702      cycles                    #    2.870 GHz                    
   591,218,425,219      instructions              #    1.39  insns per cycle        
   124,816,600,210      branches                  #  839.653 M/sec                  
       475,639,059      branch-misses             #    0.38% of all branches        

      75.314408592 seconds time elapsed

AIEngine
********

.. code:: python

  def anomaly_callback(flow):
    print("rule on HTTP %s" % str(flow))

  if __name__ == '__main__':

    st = StackLan()

    http = DomainNameManager() 
    rm = RegexManager()
    r = Regex("my cmd.exe", "cmd.exe")
    r.callback = anomaly_callback

    d1 = DomainName("Generic net",".net")
    d2 = DomainName("Generic com",".com")
    d3 = DomainName("Generic org",".org")
 
    http.add_domain_name(d1) 
    http.add_domain_name(d2) 
    http.add_domain_name(d3) 

    d1.regex_manager = rm
    d2.regex_manager = rm
    d3.regex_manager = rm

    rm.add_regex(r)

    st.set_domain_name_manager(http, "HTTPProtocol")

    st.set_dynamic_allocated_memory(True)
    
    with pyaiengine.PacketDispatcher("/pcaps/iscx/testbed-17jun.pcap") as pd:
            pd.stack = st
            pd.run()

    sys.exit(0)

.. code:: bash

   Performance counter stats for 'python http_cmd.exe.py':

      23225.111783      task-clock (msec)         #    0.852 CPUs utilized          
            65,407      context-switches          #    0.003 M/sec                  
               229      cpu-migrations            #    0.010 K/sec                  
            11,765      page-faults               #    0.507 K/sec                  
    65,522,914,305      cycles                    #    2.821 GHz                    
   107,810,319,801      instructions              #    1.65  insns per cycle        
    28,075,084,777      branches                  # 1208.825 M/sec                  
       282,404,757      branch-misses             #    1.01% of all branches        

      27.252074897 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 214,846M |     385,107M |      71 |
+-------------+----------+--------------+---------+
| Suricata(9) | 385,297M |     591,218M |      28 |
+-------------+----------+--------------+---------+
| Suricata(1) | 426,698M |     591,840M |      75 |
+-------------+----------+--------------+---------+
| AIEngine    |  65,522M |     107,810M |      27 |
+-------------+----------+--------------+---------+

Tests I with 31.000 rules
~~~~~~~~~~~~~~~~~~~~~~~~~

On this section we evalute aproximatelly 31.000 rules in order to compare the different systems.
Basically we load 31.000 different domains on each engine and loaded into memory and compare the performance.

Snort
*****

.. code:: bash
   
   alert tcp any any -> any 80 (content:"lb.usemaxserver.de"; msg:"Traffic"; sid:1; rev:1;)
   ....

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

     239911.454192      task-clock (msec)         #    0.994 CPUs utilized          
             1,866      context-switches          #    0.008 K/sec                  
                29      cpu-migrations            #    0.000 K/sec                  
           275,912      page-faults               #    0.001 M/sec                  
   730,183,866,577      cycles                    #    3.044 GHz                    
   523,549,153,058      instructions              #    0.72  insns per cycle        
   151,703,407,200      branches                  #  632.331 M/sec                  
       784,133,786      branch-misses             #    0.52% of all branches        

     241.344591225 seconds time elapsed

Suricata
********

.. code:: bash

   alert http any any -> any any (content:"lb.usemaxserver.de"; http_host; msg:"Traffic"; sid:1; rev:1;)
   ....

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -r /pcaps/iscx/testbed-17jun.pcap -c suricata.yaml':

     129366.651117      task-clock (msec)         #    3.812 CPUs utilized          
         1,484,897      context-switches          #    0.011 M/sec                  
           115,294      cpu-migrations            #    0.891 K/sec                  
           347,011      page-faults               #    0.003 M/sec                  
   354,238,365,666      cycles                    #    2.738 GHz                    
   330,226,571,287      instructions              #    0.93  insns per cycle        
    81,479,451,099      branches                  #  629.834 M/sec                  
       598,088,820      branch-misses             #    0.73% of all branches        

      33.935354390 seconds time elapsed

With one single packet thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     137079.150338      task-clock (msec)         #    1.872 CPUs utilized          
           101,577      context-switches          #    0.741 K/sec                  
             1,481      cpu-migrations            #    0.011 K/sec                  
           291,789      page-faults               #    0.002 M/sec                  
   370,552,220,742      cycles                    #    2.703 GHz                    
   443,891,171,842      instructions              #    1.20  insns per cycle        
   112,343,969,730      branches                  #  819.555 M/sec                  
       518,724,581      branch-misses             #    0.46% of all branches        

      73.230102972 seconds time elapsed

nDPI
****

.. code:: bash

   host:"lb.usemaxserver.de"@MyProtocol

.. code:: bash

   Performance counter stats for './ndpiReader -p http_ndpi.rules -i /pcaps/iscx/testbed-17jun.pcap':

      21913.851054      task-clock (msec)         #    0.779 CPUs utilized          
            59,037      context-switches          #    0.003 M/sec                  
                83      cpu-migrations            #    0.004 K/sec                  
           716,580      page-faults               #    0.033 M/sec                  
    59,048,108,901      cycles                    #    2.695 GHz                    
    63,994,766,870      instructions              #    1.08  insns per cycle        
    15,288,226,665      branches                  #  697.651 M/sec                  
       284,549,749      branch-misses             #    1.86% of all branches        

      28.147959104 seconds time elapsed

AIEngine
********

.. code:: bash

   h = pyaiengine.DomainName("domain_1" % i, "b.usemaxserver.de")
   h.callback = http_callback
   dm.add_domain_name(h)
   ....

The version used on this test is 1.8.x (master)

.. code:: bash

   Performance counter stats for 'python demo5.py':

      19910.218719      task-clock (msec)         #    0.753 CPUs utilized          
            86,698      context-switches          #    0.004 M/sec                  
             2,013      cpu-migrations            #    0.101 K/sec                  
            18,189      page-faults               #    0.914 K/sec                  
    53,190,204,685      cycles                    #    2.672 GHz                    
    64,288,671,886      instructions              #    1.21  insns per cycle        
    13,373,079,549      branches                  #  671.669 M/sec                  
       169,799,323      branch-misses             #    1.27% of all branches        

      26.452084280 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 730,183M |     523,549M |     241 |
+-------------+----------+--------------+---------+
| Suricata(9) | 354,238M |     330,226M |      33 |
+-------------+----------+--------------+---------+
| Suricata(1) | 370,552M |     443,891M |      73 |
+-------------+----------+--------------+---------+
| nDPI        |  59,048M |      63,994M |      28 |
+-------------+----------+--------------+---------+
| AIEngine    |  53,190M |      64,288M |      26 |
+-------------+----------+--------------+---------+

Now we are going to make a complex rule.

The idea is to analyze the HTTP uri and search for a word in our case "exe".

Snort
*****

.. code:: bash

   alert tcp any any -> any 80 (content:"lb.usemaxserver.de"; uricontent:"exe"; msg:"Traffic"; sid:1; rev:1;)
   ....

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

      76455.475108      task-clock (msec)         #    0.981 CPUs utilized          
             3,594      context-switches          #    0.047 K/sec                  
                99      cpu-migrations            #    0.001 K/sec                  
           111,397      page-faults               #    0.001 M/sec                  
   229,619,037,994      cycles                    #    3.003 GHz                    
   405,962,474,441      instructions              #    1.77  insns per cycle        
   106,466,397,876      branches                  # 1392.528 M/sec                  
       594,124,564      branch-misses             #    0.56% of all branches        

      77.938067412 seconds time elapsed

Suricata
********

.. code:: bash

   alert http any any -> any any (content:"lb.usemaxserver.de"; http_host; conent:"exe"; http_uri; msg:"Traffic"; sid:1; rev:1;)
   ....

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -r /pcaps/iscx/testbed-17jun.pcap -c suricata.yaml':

     123037.997614      task-clock (msec)         #    3.475 CPUs utilized          
         1,765,919      context-switches          #    0.014 M/sec                  
           148,475      cpu-migrations            #    0.001 M/sec                  
           353,585      page-faults               #    0.003 M/sec                  
   332,912,328,748      cycles                    #    2.706 GHz                    
   332,626,051,284      instructions              #    1.00  insns per cycle        
    81,934,929,717      branches                  #  665.932 M/sec                  
       592,853,289      branch-misses             #    0.72% of all branches        

      35.411677796 seconds time elapsed

With one single packet thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     111133.956719      task-clock (msec)         #    1.843 CPUs utilized          
           111,599      context-switches          #    0.001 M/sec                  
             1,077      cpu-migrations            #    0.010 K/sec                  
           306,054      page-faults               #    0.003 M/sec                  
   310,127,777,799      cycles                    #    2.791 GHz                    
   412,013,001,291      instructions              #    1.33  insns per cycle        
   103,895,197,621      branches                  #  934.865 M/sec                  
       508,998,872      branch-misses             #    0.49% of all branches        

      60.309266689 seconds time elapsed

AIEngine
********

.. code:: bash

   rm = pyaiengine.RegexManager()
   r = pyaiengine.Regex("on the uri", "^.*(exe).*$")
   rm.add_regex(r)

   h = pyaiengine.DomainName("domain_1" % i, "b.usemaxserver.de")
   h.callback = http_callback
   h.http_uri_regex_manager = rm
   dm.add_domain_name(h)
   ....

The version used on this test is 1.8.x (master)

.. code:: bash

   Performance counter stats for 'python demo5.py':

      19910.218719      task-clock (msec)         #    0.753 CPUs utilized          
            86,698      context-switches          #    0.004 M/sec                  
             2,013      cpu-migrations            #    0.101 K/sec                  
            18,189      page-faults               #    0.914 K/sec                  
    53,190,204,685      cycles                    #    2.672 GHz                    
    64,288,671,886      instructions              #    1.21  insns per cycle        
    13,373,079,549      branches                  #  671.669 M/sec                  
       169,799,323      branch-misses             #    1.27% of all branches        

      26.452084280 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 229,619M |     405,962M |      77 |
+-------------+----------+--------------+---------+
| Suricata(9) | 332,912M |     332,626M |      35 |
+-------------+----------+--------------+---------+
| Suricata(1) | 310,127M |     412,013M |      60 |
+-------------+----------+--------------+---------+
| AIEngine    |  53,190M |      64,288M |      26 |
+-------------+----------+--------------+---------+


Another tests by making more complex the rule 

The idea is to analyze the HTTP uri and search for different words(exe, bat and png).

Snort
*****

.. code:: bash

   alert tcp any any -> any 80 (content:"lb.usemaxserver.de"; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;)
   ...

.. code:: bash 

   Run time for packet processing was 87.8067 seconds
   Snort processed 17310684 packets.
   Snort ran for 0 days 0 hours 1 minutes 27 seconds
      Pkts/min:     17310684
      Pkts/sec:       198973

   ...

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

     332419.465677      task-clock (msec)         #    0.996 CPUs utilized          
             1,897      context-switches          #    0.006 K/sec                  
                70      cpu-migrations            #    0.000 K/sec                  
           298,836      page-faults               #    0.899 K/sec                  
   870,336,957,271      cycles                    #    2.618 GHz                    
   527,446,002,353      instructions              #    0.61  insns per cycle        
   152,281,712,268      branches                  #  458.101 M/sec                  
       771,410,918      branch-misses             #    0.51% of all branches        

     333.678629049 seconds time elapsed

The packet processing takes about 88 seconds but the full load of the rules takes a long time, probably due to the use of the pcre.

Suricata
********

.. code:: bash

   alert http any any -> any any (content:"lb.usemaxserver.de"; http_host; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;)
   ...

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     133747.431539      task-clock (msec)         #    3.796 CPUs utilized          
         1,507,433      context-switches          #    0.011 M/sec                  
           123,806      cpu-migrations            #    0.926 K/sec                  
           374,176      page-faults               #    0.003 M/sec                  
   362,046,514,184      cycles                    #    2.707 GHz                    
   335,210,037,408      instructions              #    0.93  insns per cycle        
    82,517,301,739      branches                  #  616.964 M/sec                  
       598,287,782      branch-misses             #    0.73% of all branches        

      35.237027328 seconds time elapsed

Running suricata with one single thread (same has AIEngine)

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/iscx/testbed-17jun.pcap':

     122334.651821      task-clock (msec)         #    1.864 CPUs utilized          
            97,856      context-switches          #    0.800 K/sec                  
             1,073      cpu-migrations            #    0.009 K/sec                  
           300,312      page-faults               #    0.002 M/sec                  
   344,624,244,835      cycles                    #    2.817 GHz                    
   439,114,648,308      instructions              #    1.27  insns per cycle        
   110,921,840,589      branches                  #  906.708 M/sec                  
       513,286,800      branch-misses             #    0.46% of all branches        

      65.636419341 seconds time elapsed

AIEngine
********

The version used on this test is 1.8.x(master)

By using the or exclusive on the regex

.. code:: python

   rm = pyaiengine.RegexManager()
   r = pyaiengine.Regex("on the uri", "^.*(exe|png|bat).*$")
   rm.add_regex(r)

   h = pyaiengine.DomainName("domain_1" % i, "b.usemaxserver.de")
   h.callback = http_callback
   h.http_uri_regex_manager = rm
   dm.add_domain_name(h)
   ....

.. code:: bash

   Performance counter stats for 'python demo5.py':

      20363.823330      task-clock (msec)         #    0.768 CPUs utilized          
            83,882      context-switches          #    0.004 M/sec                  
             2,623      cpu-migrations            #    0.129 K/sec                  
            18,598      page-faults               #    0.913 K/sec                  
    53,956,703,007      cycles                    #    2.650 GHz                    
    64,766,501,001      instructions              #    1.20  insns per cycle        
    13,439,249,412      branches                  #  659.957 M/sec                  
       172,814,792      branch-misses             #    1.29% of all branches        

      26.520199336 seconds time elapsed

Creating three different regex

.. code:: python

   rm = pyaiengine.RegexManager()
   r1 = pyaiengine.Regex("on the uri1", "^.*(exe).*$")
   r2 = pyaiengine.Regex("on the uri2", "^.*(png).*$")
   r3 = pyaiengine.Regex("on the uri3", "^.*(bat).*$")
   rm.add_regex(r1)
   rm.add_regex(r2)
   rm.add_regex(r3)


.. code:: bash

   Performance counter stats for 'python demo5.py':

      19791.762693      task-clock (msec)         #    0.754 CPUs utilized          
            86,195      context-switches          #    0.004 M/sec                  
             2,433      cpu-migrations            #    0.123 K/sec                  
            18,554      page-faults               #    0.937 K/sec                  
    52,834,415,115      cycles                    #    2.670 GHz                    
    64,730,476,443      instructions              #    1.23  insns per cycle        
    13,448,104,589      branches                  #  679.480 M/sec                  
       173,179,979      branch-misses             #    1.29% of all branches        

      26.252955656 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 870,336M |     527,446M |      87 |
+-------------+----------+--------------+---------+
| Suricata(9) | 362,046M |     335,210M |      35 |
+-------------+----------+--------------+---------+
| Suricata(1) | 344,624M |     439,114M |      65 |
+-------------+----------+--------------+---------+
| AIEngine    |  52,834M |      64,730M |      26 |
+-------------+----------+--------------+---------+

Test II
.......

In this section we are going to perform the second pcap (https://download.netresec.com/pcap/ists-12/2015-03-07/)


Test II processing traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~

Same principal as the previous test, execute the engines without any rules or logic on them.

Snort
*****

.. code:: bash

   Performance counter stats for './snort -c snort.conf -r /pcaps/ists/snort.sample.142574.pcap':

      20239.719847      task-clock (msec)         #    0.896 CPUs utilized          
            13,720      context-switches          #    0.678 K/sec                  
                34      cpu-migrations            #    0.002 K/sec                  
            64,599      page-faults               #    0.003 M/sec                  
    60,253,485,863      cycles                    #    2.977 GHz                    
   103,576,923,708      instructions              #    1.72  insns per cycle        
    23,248,922,048      branches                  # 1148.678 M/sec                  
       145,650,931      branch-misses             #    0.63% of all branches        

      22.594726539 seconds time elapsed

Tshark
******

.. code:: bash

   Performance counter stats for 'tshark -q -z conv,tcp -r /pcaps/ists/snort.sample.142574.pcap':

     172043.327012      task-clock (msec)         #    0.986 CPUs utilized          
             8,925      context-switches          #    0.052 K/sec                  
                54      cpu-migrations            #    0.000 K/sec                  
         2,246,437      page-faults               #    0.013 M/sec                  
   507,338,842,395      cycles                    #    2.949 GHz                    
   490,075,423,649      instructions              #    0.97  insns per cycle        
   110,140,671,629      branches                  #  640.191 M/sec                  
       908,018,085      branch-misses             #    0.82% of all branches        

     174.515503354 seconds time elapsed

Suricata
********

With 9 packet processing threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/ists/snort.sample.142574.pcap':

      49619.488693      task-clock (msec)         #    2.567 CPUs utilized          
         2,146,042      context-switches          #    0.043 M/sec                  
           274,824      cpu-migrations            #    0.006 M/sec                  
            41,016      page-faults               #    0.827 K/sec                  
   133,760,571,310      cycles                    #    2.696 GHz                    
   137,849,439,654      instructions              #    1.03  insns per cycle        
    29,990,793,429      branches                  #  604.416 M/sec                  
       240,231,193      branch-misses             #    0.80% of all branches        

      19.327455566 seconds time elapsed

With one packet processing thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/ists/snort.sample.142574.pcap':

      27516.148594      task-clock (msec)         #    1.761 CPUs utilized          
            16,899      context-switches          #    0.614 K/sec                  
               152      cpu-migrations            #    0.006 K/sec                  
            28,250      page-faults               #    0.001 M/sec                  
    78,898,553,305      cycles                    #    2.867 GHz                    
   117,482,892,525      instructions              #    1.49  insns per cycle        
    26,234,850,954      branches                  #  953.435 M/sec                  
       173,307,394      branch-misses             #    0.66% of all branches        

      15.622774603 seconds time elapsed

nDPI
****

.. code:: bash

   Performance counter stats for './ndpiReader -i /pcaps/ists/snort.sample.142574.pcap':

       8334.169519      task-clock (msec)         #    1.000 CPUs utilized          
                15      context-switches          #    0.002 K/sec                  
                 4      cpu-migrations            #    0.000 K/sec                  
           117,034      page-faults               #    0.014 M/sec                  
    24,556,541,541      cycles                    #    2.946 GHz                    
    35,137,201,115      instructions              #    1.43  insns per cycle        
     7,695,905,629      branches                  #  923.416 M/sec                  
       109,421,601      branch-misses             #    1.42% of all branches        

       8.336547614 seconds time elapsed

AIengine
********

.. code:: bash

   Performance counter stats for './aiengine -i /pcaps/ists/snort.sample.142574.pcap -o':

       8918.852145      task-clock (msec)         #    0.998 CPUs utilized          
                84      context-switches          #    0.009 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            23,883      page-faults               #    0.003 M/sec                  
    26,603,158,389      cycles                    #    2.983 GHz                    
    35,019,717,270      instructions              #    1.32  insns per cycle        
     6,779,154,714      branches                  #  760.093 M/sec                  
        52,531,344      branch-misses             #    0.77% of all branches        

       8.932988996 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       |  60.253M |     103.576M |      22 |
+-------------+----------+--------------+---------+
| Tshark      | 507.338M |     490.075M |     174 |
+-------------+----------+--------------+---------+
| Suricata(9) | 133.760M |     137.849M |      19 |
+-------------+----------+--------------+---------+
| Suricata(1) |  78.898M |     117.482M |      15 |
+-------------+----------+--------------+---------+
| nDPI        |  24.556M |      35.137M |       8 |
+-------------+----------+--------------+---------+
| AIEngine    |  26.603M |      35.019M |       8 |
+-------------+----------+--------------+---------+

Tests II with rules
~~~~~~~~~~~~~~~~~~~

The rule that we are going to use consists on find the string "cmd.exe" on the payload of all the TCP traffic.

Snort
*****

.. code:: bash

   alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1)

.. code:: bash

   Performance counter stats for './snort -c snort.conf -r /pcaps/ists/snort.sample.142574.pcap':

      57274.705850      task-clock (msec)         #    0.978 CPUs utilized          
             1,475      context-switches          #    0.026 K/sec                  
                30      cpu-migrations            #    0.001 K/sec                  
            74,055      page-faults               #    0.001 M/sec                  
   170,108,684,940      cycles                    #    2.970 GHz                    
   249,563,724,967      instructions              #    1.47  insns per cycle        
    44,950,506,837      branches                  #  784.823 M/sec                  
       166,126,757      branch-misses             #    0.37% of all branches        

      58.554078720 seconds time elapsed

Suricata
********

.. code:: bash

   alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash 

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/ists/snort.sample.142574.pcap':

      55413.061279      task-clock (msec)         #    3.707 CPUs utilized          
         1,832,228      context-switches          #    0.033 M/sec                  
           208,029      cpu-migrations            #    0.004 M/sec                  
           178,505      page-faults               #    0.003 M/sec                  
   152,711,396,141      cycles                    #    2.756 GHz                    
   169,560,770,675      instructions              #    1.11  insns per cycle        
    33,695,213,952      branches                  #  608.073 M/sec                  
       254,682,262      branch-misses             #    0.76% of all branches        

      14.948748524 seconds time elapsed

With one packet processing thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/ists/snort.sample.142574.pcap':

      37532.872741      task-clock (msec)         #    1.689 CPUs utilized          
            20,394      context-switches          #    0.543 K/sec                  
               166      cpu-migrations            #    0.004 K/sec                  
            28,466      page-faults               #    0.758 K/sec                  
   112,217,535,031      cycles                    #    2.990 GHz                    
   171,185,106,113      instructions              #    1.53  insns per cycle        
    35,464,805,544      branches                  #  944.900 M/sec                  
       178,621,523      branch-misses             #    0.50% of all branches        

      22.228136143 seconds time elapsed

AIEngine
********

Rule: "cmd.exe"

.. code:: bash

   Performance counter stats for './aiengine -R -r cmd.exe -i /pcaps/ists/snort.sample.142574.pcap':

      12141.379451      task-clock (msec)         #    1.000 CPUs utilized          
                26      context-switches          #    0.002 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            22,007      page-faults               #    0.002 M/sec                  
    39,543,086,875      cycles                    #    3.257 GHz                    
    87,423,235,646      instructions              #    2.21  insns per cycle        
    25,601,360,813      branches                  # 2108.604 M/sec                  
       102,191,892      branch-misses             #    0.40% of all branches        

      12.145469760 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 170.108M |     249.563M |      58 |
+-------------+----------+--------------+---------+
| Suricata(9) | 152.711M |     169.560M |      14 |
+-------------+----------+--------------+---------+
| Suricata(1) | 112.217M |     171.185M |      22 |
+-------------+----------+--------------+---------+
| AIEngine    |  39.543M |      87.423M |      12 |
+-------------+----------+--------------+---------+

Snort
*****

A simliar rules as before but just trying to help a bit to Snort.

.. code:: bash

   alert tcp any any -> any 80 (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash

   Performance counter stats for './snort -c snort.conf -r /pcaps/ists/snort.sample.142574.pcap':

      18891.239382      task-clock (msec)         #    0.961 CPUs utilized          
               277      context-switches          #    0.015 K/sec                  
                12      cpu-migrations            #    0.001 K/sec                  
            75,406      page-faults               #    0.004 M/sec                  
    61,694,270,612      cycles                    #    3.266 GHz                    
   108,319,753,502      instructions              #    1.76  insns per cycle        
    24,001,563,160      branches                  # 1270.513 M/sec                  
       138,490,930      branch-misses             #    0.58% of all branches        

      19.653087466 seconds time elapsed

Suricata
********

Change the rule just for HTTP traffic

.. code:: bash

   alert http any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/ists/snort.sample.142574.pcap':

      55218.532532      task-clock (msec)         #    3.725 CPUs utilized          
         1,830,002      context-switches          #    0.033 M/sec                  
           194,003      cpu-migrations            #    0.004 M/sec                  
           190,322      page-faults               #    0.003 M/sec                  
   152,046,385,482      cycles                    #    2.754 GHz                    
   168,972,894,992      instructions              #    1.11  insns per cycle        
    33,590,489,520      branches                  #  608.319 M/sec                  
       250,682,512      branch-misses             #    0.75% of all branches        

      14.825638711 seconds time elapsed

With one processing packet thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/ists/snort.sample.142574.pcap':

      37795.997821      task-clock (msec)         #    1.689 CPUs utilized          
            18,530      context-switches          #    0.490 K/sec                  
               211      cpu-migrations            #    0.006 K/sec                  
            28,111      page-faults               #    0.744 K/sec                  
   112,302,644,819      cycles                    #    2.971 GHz                    
   171,212,241,453      instructions              #    1.52  insns per cycle        
    35,470,318,890      branches                  #  938.468 M/sec                  
       178,287,454      branch-misses             #    0.50% of all branches        

      22.376103005 seconds time elapsed

AIEngine
********

The python code used is the same as the previous examples

.. code:: bash

   Performance counter stats for 'python http_cmd.exe.py':

       9364.809266      task-clock (msec)         #    0.995 CPUs utilized          
               215      context-switches          #    0.023 K/sec                  
                 4      cpu-migrations            #    0.000 K/sec                  
            27,663      page-faults               #    0.003 M/sec                  
    29,507,034,876      cycles                    #    3.151 GHz                    
    42,257,378,116      instructions              #    1.43  insns per cycle        
     9,369,675,470      branches                  # 1000.520 M/sec                  
        67,494,718      branch-misses             #    0.72% of all branches        

       9.407827617 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       |  61.694M |     108.319M |      19 |
+-------------+----------+--------------+---------+
| Suricata(9) | 152.046M |     168.972M |      14 |
+-------------+----------+--------------+---------+
| Suricata(1) | 112.302M |     171.212M |      22 |
+-------------+----------+--------------+---------+
| AIEngine    |  29.507M |      42.257M |       9 |
+-------------+----------+--------------+---------+

Tests II with 31.000 rules
~~~~~~~~~~~~~~~~~~~~~~~~~~

On this section we evalute aproximatelly 31.000 rules in order to compare the different systems.
We will execute a complex rule directly instead of test a basic one as did on previous tests

Be aware that the portion of HTTP on this pcap is different and the rules generated are for HTTP traffic basically.

Snort
*****

.. code:: bash

   alert tcp any any -> any 80 (content:"lb.usemaxserver.de"; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;) 
   ...

.. code:: bash

   Run time for packet processing was 27.3672 seconds
   Snort processed 14021863 packets.
   Snort ran for 0 days 0 hours 0 minutes 27 seconds
      Pkts/sec:       519328
   ...

   Performance counter stats for './snort -c snort.conf -r /pcaps/ists/snort.sample.142574.pcap':

     188025.287538      task-clock (msec)         #    0.987 CPUs utilized          
            13,598      context-switches          #    0.072 K/sec                  
                45      cpu-migrations            #    0.000 K/sec                  
           276,745      page-faults               #    0.001 M/sec                  
   589,679,607,434      cycles                    #    3.136 GHz                    
   247,581,636,213      instructions              #    0.42  insns per cycle        
    75,802,520,939      branches                  #  403.151 M/sec                  
       332,483,691      branch-misses             #    0.44% of all branches        

     190.513077863 seconds time elapsed

Suricata
********

.. code:: bash

   alert http any any -> any any (content:"lb.usemaxserver.de"; http_host; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;)
   ...

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/ists/snort.sample.142574.pcap':

      63154.209557      task-clock (msec)         #    2.605 CPUs utilized          
         1,939,476      context-switches          #    0.031 M/sec                  
           224,117      cpu-migrations            #    0.004 M/sec                  
           273,255      page-faults               #    0.004 M/sec                  
   175,477,179,743      cycles                    #    2.779 GHz                    
   221,833,693,652      instructions              #    1.26  insns per cycle        
    55,880,187,462      branches                  #  884.821 M/sec                  
       288,292,750      branch-misses             #    0.52% of all branches        

      24.242640026 seconds time elapsed

Running suricata with one single thread

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml --runmode single -r /pcaps/ists/snort.sample.142574.pcap':

      43689.975427      task-clock (msec)         #    1.470 CPUs utilized          
            20,138      context-switches          #    0.461 K/sec                  
               171      cpu-migrations            #    0.004 K/sec                  
           231,460      page-faults               #    0.005 M/sec                  
   129,790,681,545      cycles                    #    2.971 GHz                    
   219,021,005,746      instructions              #    1.69  insns per cycle        
    56,543,491,574      branches                  # 1294.198 M/sec                  
       214,892,514      branch-misses             #    0.38% of all branches        

      29.723236744 seconds time elapsed

AIEngine
********

.. code:: python

   rm = pyaiengine.RegexManager()
   r = pyaiengine.Regex("on the uri", "^.*(exe|png|bat).*$")
   rm.add_regex(r)

   h = pyaiengine.DomainName("domain_1" % i, "b.usemaxserver.de")
   h.callback = http_callback
   h.http_uri_regex_manager = rm
   dm.add_domain_name(h)
   ....

.. code:: bash

   Performance counter stats for 'python demo5.py':

       9012.254691      task-clock (msec)         #    0.991 CPUs utilized          
               358      context-switches          #    0.040 K/sec                  
                 7      cpu-migrations            #    0.001 K/sec                  
            34,290      page-faults               #    0.004 M/sec                  
    27,711,492,237      cycles                    #    3.075 GHz                    
    37,007,993,406      instructions              #    1.34  insns per cycle        
     7,359,467,289      branches                  #  816.607 M/sec                  
        54,095,400      branch-misses             #    0.74% of all branches        

       9.091943766 seconds time elapsed

Now to get the best of the engine, we load the same domains on SSL traffic for evaluate the impact. So 31000 HTTP domains and 31000 SSL domains in total

.. code:: bash

   Performance counter stats for 'python demo5.py':

       9493.778542      task-clock (msec)         #    0.905 CPUs utilized          
             6,514      context-switches          #    0.686 K/sec                  
                12      cpu-migrations            #    0.001 K/sec                  
            41,007      page-faults               #    0.004 M/sec                  
    28,777,946,232      cycles                    #    3.031 GHz                    
    38,475,050,285      instructions              #    1.34  insns per cycle        
     7,665,290,640      branches                  #  807.401 M/sec                  
        56,090,118      branch-misses             #    0.73% of all branches        

      10.486534296 seconds time elapsed

And another example by dumping the network flows into a file

.. code:: python

    d = datamng.databaseFileAdaptor("network_data.txt")

    st.set_tcp_database_adaptor(d, 32)

.. code:: bash

   Performance counter stats for 'python demo5.py':

      15905.181590      task-clock (msec)         #    0.994 CPUs utilized          
                39      context-switches          #    0.002 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            41,011      page-faults               #    0.003 M/sec                  
    52,568,018,546      cycles                    #    3.305 GHz                    
    81,161,491,461      instructions              #    1.54  insns per cycle        
    16,891,722,781      branches                  # 1062.026 M/sec                  
       118,268,844      branch-misses             #    0.70% of all branches        

      16.000191844 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 589.679M |     247.581M |      27 |
+-------------+----------+--------------+---------+
| Suricata(9) | 175.477M |     221.833M |      24 |
+-------------+----------+--------------+---------+
| Suricata(1) | 129.790M |     219.021M |      29 |
+-------------+----------+--------------+---------+
| AIEngine    |  27.711M |      37.007M |       9 |
+-------------+----------+--------------+---------+

Test III
........

In this section we are going to perform the thrid pcap (https://www.unsw.adfa.edu.au/australian-centre-for-cyber-security/cybersecurity/ADFA-NB15-Datasets/)


Test III processing traffic
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Same principal as the previous test, execute the engines without any rules or logic on them.

Snort
*****

.. code:: bash

  Performance counter stats for './snort -c snort.conf -r /pcaps/unsw-nb15/data1to10.pcap':

      43311.137970      task-clock (msec)         #    0.909 CPUs utilized          
            68,285      context-switches          #    0.002 M/sec                  
                72      cpu-migrations            #    0.002 K/sec                  
            77,871      page-faults               #    0.002 M/sec                  
   129,441,905,372      cycles                    #    2.989 GHz                    
   228,452,666,316      instructions              #    1.76  insns per cycle        
    50,696,805,653      branches                  # 1170.526 M/sec                  
       277,402,355      branch-misses             #    0.55% of all branches        

      47.652751113 seconds time elapsed

Tshark
******

.. code:: bash

   Performance counter stats for 'tshark -q -z conv,tcp -r /pcaps/unsw-nb15/data1to10.pcap':

     254269.848485      task-clock (msec)         #    0.644 CPUs utilized          
            56,111      context-switches          #    0.221 K/sec                  
             4,753      cpu-migrations            #    0.019 K/sec                  
         4,760,630      page-faults               #    0.019 M/sec                  
   680,886,431,407      cycles                    #    2.678 GHz                    
   984,061,462,610      instructions              #    1.45  insns per cycle        
   220,283,816,702      branches                  #  866.339 M/sec                  
     1,780,070,602      branch-misses             #    0.81% of all branches        

     394.760217311 seconds time elapsed

Suricata
********

With 9 packet processing threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/unsw-nb15/data1to10.pcap':

     118848.792160      task-clock (msec)         #    3.032 CPUs utilized          
         3,200,591      context-switches          #    0.027 M/sec                  
           212,088      cpu-migrations            #    0.002 M/sec                  
            51,566      page-faults               #    0.434 K/sec                  
   327,130,620,161      cycles                    #    2.752 GHz                    
   346,451,098,762      instructions              #    1.06  insns per cycle        
    77,424,252,129      branches                  #  651.452 M/sec                  
       515,720,079      branch-misses             #    0.67% of all branches        

      39.195811425 seconds time elapsed

With one packet processing thread

.. code:: bash

  Performance counter stats for './suricata --runmode single -c suricata.yaml -r /pcaps/unsw-nb15/data1to10.pcap':

      87802.781250      task-clock (msec)         #    1.828 CPUs utilized          
           122,392      context-switches          #    0.001 M/sec                  
               859      cpu-migrations            #    0.010 K/sec                  
            37,786      page-faults               #    0.430 K/sec                  
   247,338,260,771      cycles                    #    2.817 GHz                    
   347,502,402,105      instructions              #    1.40  insns per cycle        
    79,836,664,496      branches                  #  909.273 M/sec                  
       441,950,917      branch-misses             #    0.55% of all branches        

      48.040050295 seconds time elapsed

nDPI
****

.. code:: bash

   Performance counter stats for './ndpiReader -i /pcaps/unsw-nb15/data1to10.pcap':

      28681.266817      task-clock (msec)         #    0.713 CPUs utilized          
           133,389      context-switches          #    0.005 M/sec                  
               393      cpu-migrations            #    0.014 K/sec                  
            72,403      page-faults               #    0.003 M/sec                  
    76,471,278,050      cycles                    #    2.666 GHz                    
   100,179,916,622      instructions              #    1.31  insns per cycle        
    21,808,590,178      branches                  #  760.378 M/sec                  
       380,193,138      branch-misses             #    1.74% of all branches        

      40.213638823 seconds time elapsed

AIengine
********

.. code:: bash

   Performance counter stats for './aiengine -i /pcaps/unsw-nb15/data1to10.pcap -o':

      26601.771136      task-clock (msec)         #    0.682 CPUs utilized          
           144,328      context-switches          #    0.005 M/sec                  
               202      cpu-migrations            #    0.008 K/sec                  
             4,133      page-faults               #    0.155 K/sec                  
    72,037,339,115      cycles                    #    2.708 GHz                    
    94,503,591,441      instructions              #    1.31  insns per cycle        
    17,851,281,193      branches                  #  671.056 M/sec                  
       141,396,866      branch-misses             #    0.79% of all branches        

      38.979087989 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 129.441M |     228.452M |      47 |
+-------------+----------+--------------+---------+
| Tshark      | 680.886M |     984.061M |     394 |
+-------------+----------+--------------+---------+
| Suricata(9) | 327.130M |     346.451M |      39 |
+-------------+----------+--------------+---------+
| Suricata(1) | 247.338M |     347.502M |      48 |
+-------------+----------+--------------+---------+
| nDPI        |  76.471M |     100.179M |      40 |
+-------------+----------+--------------+---------+
| AIEngine    |  72.037M |      94.503M |      38 |
+-------------+----------+--------------+---------+

Tests III with rules
~~~~~~~~~~~~~~~~~~~~

The rule that we are going to use consists on find the string "cmd.exe" on the payload of all the TCP traffic.

Snort
*****

.. code:: bash

   alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1)

.. code:: bash

   Performance counter stats for './snort -c snort.conf -r /pcaps/unsw-nb15/data1to10.pcap':

     113174.292621      task-clock (msec)         #    0.988 CPUs utilized          
             1,137      context-switches          #    0.010 K/sec                  
                33      cpu-migrations            #    0.000 K/sec                  
            19,742      page-faults               #    0.174 K/sec                  
   363,317,374,611      cycles                    #    3.210 GHz                    
   554,398,940,706      instructions              #    1.53  insns per cycle        
    99,024,544,022      branches                  #  874.974 M/sec                  
       316,157,452      branch-misses             #    0.32% of all branches        

     114.533609997 seconds time elapsed

Suricata
********

.. code:: bash

   alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash 

  Performance counter stats for './suricata -c suricata.yaml -r /pcaps/unsw-nb15/data1to10.pcap':

     145993.577359      task-clock (msec)         #    3.639 CPUs utilized          
         2,203,965      context-switches          #    0.015 M/sec                  
           159,890      cpu-migrations            #    0.001 M/sec                  
            57,647      page-faults               #    0.395 K/sec                  
   402,743,821,496      cycles                    #    2.759 GHz                    
   472,602,174,597      instructions              #    1.17  insns per cycle        
    94,330,119,507      branches                  #  646.125 M/sec                  
       535,283,885      branch-misses             #    0.57% of all branches        

      40.118781148 seconds time elapsed

With one packet processing thread

.. code:: bash

   Performance counter stats for './suricata --runmode single -c suricata.yaml -r /pcaps/unsw-nb15/data1to10.pcap':

     127436.835622      task-clock (msec)         #    1.915 CPUs utilized          
            46,945      context-switches          #    0.368 K/sec                  
               751      cpu-migrations            #    0.006 K/sec                  
            39,253      page-faults               #    0.308 K/sec                  
   365,540,852,657      cycles                    #    2.868 GHz                    
   542,494,038,852      instructions              #    1.48  insns per cycle        
   114,322,178,061      branches                  #  897.089 M/sec                  
       460,246,313      branch-misses             #    0.40% of all branches        

      66.555702401 seconds time elapsed

AIEngine
********
Rule: "cmd.exe"

.. code:: bash

   Performance counter stats for './aiengine -R -r cmd.exe -i /pcaps/unsw-nb15/data1to10.pcap':

      37115.944815      task-clock (msec)         #    0.908 CPUs utilized          
           104,495      context-switches          #    0.003 M/sec                  
                99      cpu-migrations            #    0.003 K/sec                  
             3,084      page-faults               #    0.083 K/sec                  
   106,374,950,181      cycles                    #    2.866 GHz                    
   223,900,920,532      instructions              #    2.10  insns per cycle        
    63,476,911,197      branches                  # 1710.233 M/sec                  
       285,630,090      branch-misses             #    0.45% of all branches        

      40.874818826 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 363.317M |     554.398M |     114 |
+-------------+----------+--------------+---------+
| Suricata(9) | 402.743M |     472.602M |      40 |
+-------------+----------+--------------+---------+
| Suricata(1) | 365.540M |     542.494M |      66 |
+-------------+----------+--------------+---------+
| AIEngine    | 106.374M |     223.900M |      40 |
+-------------+----------+--------------+---------+

Snort
*****

A simliar rules as before but just trying to help a bit to Snort.

.. code:: bash

   alert tcp any any -> any 80 (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash

   Performance counter stats for './snort -c snort.conf -r /pcaps/unsw-nb15/data1to10.pcap':

      45414.549383      task-clock (msec)         #    0.915 CPUs utilized          
            60,356      context-switches          #    0.001 M/sec                  
               123      cpu-migrations            #    0.003 K/sec                  
            39,676      page-faults               #    0.874 K/sec                  
   132,140,043,384      cycles                    #    2.910 GHz                    
   236,445,191,637      instructions              #    1.79  insns per cycle        
    52,395,233,434      branches                  # 1153.710 M/sec                  
       286,025,625      branch-misses             #    0.55% of all branches        

      49.623761329 seconds time elapsed

Suricata
********

Change the rule just for HTTP traffic

.. code:: bash

   alert http any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/unsw-nb15/data1to10.pcap':

     145535.618413      task-clock (msec)         #    3.655 CPUs utilized          
         2,209,103      context-switches          #    0.015 M/sec                  
           166,569      cpu-migrations            #    0.001 M/sec                  
            50,388      page-faults               #    0.346 K/sec                  
   406,123,614,420      cycles                    #    2.791 GHz                    
   473,217,415,595      instructions              #    1.17  insns per cycle        
    94,490,148,138      branches                  #  649.258 M/sec                  
       547,201,854      branch-misses             #    0.58% of all branches        

      39.816298436 seconds time elapsed

With one processing packet thread

.. code:: bash

   Performance counter stats for './suricata --runmode single -c suricata.yaml -r /pcaps/unsw-nb15/data1to10.pcap':

     129430.106611      task-clock (msec)         #    1.909 CPUs utilized          
            47,415      context-switches          #    0.366 K/sec                  
               762      cpu-migrations            #    0.006 K/sec                  
            34,570      page-faults               #    0.267 K/sec                  
   372,949,204,838      cycles                    #    2.881 GHz                    
   546,431,081,136      instructions              #    1.47  insns per cycle        
   115,355,650,254      branches                  #  891.258 M/sec                  
       458,226,227      branch-misses             #    0.40% of all branches        

      67.786835982 seconds time elapsed

AIEngine
********

The python code used is the same as the previous examples

.. code:: bash

   Performance counter stats for 'python http_cmd.exe.py':

      25794.834017      task-clock (msec)         #    0.667 CPUs utilized          
           145,995      context-switches          #    0.006 M/sec                  
               214      cpu-migrations            #    0.008 K/sec                  
             5,730      page-faults               #    0.222 K/sec                  
    72,392,158,642      cycles                    #    2.806 GHz                    
    96,269,196,405      instructions              #    1.33  insns per cycle        
    18,609,229,265      branches                  #  721.432 M/sec                  
       145,724,922      branch-misses             #    0.78% of all branches        

      38.662330346 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 132.140M |     236.445M |      49 |
+-------------+----------+--------------+---------+
| Suricata(9) | 406.123M |     473.217M |      39 |
+-------------+----------+--------------+---------+
| Suricata(1) | 372.949M |     546.431M |      67 |
+-------------+----------+--------------+---------+
| AIEngine    |  72.392M |      96.269M |      38 |
+-------------+----------+--------------+---------+

Tests III with 31.000 rules
~~~~~~~~~~~~~~~~~~~~~~~~~~~

On this section we evalute aproximatelly 31.000 rules in order to compare the different systems.
We will execute a complex rule directly instead of test a basic one as did on previous tests

Be aware that the portion of HTTP on this pcap is different and the rules generated are for HTTP traffic basically.

Snort
*****
TODO
.. code:: bash

   alert tcp any any -> any 80 (content:"example.int"; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;) 
   alert tcp any any -> any 80 (content:"lb.usemaxserver.de"; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;) 
   ...

.. code:: bash

   Run time for packet processing was 48.5940 seconds
   Snort processed 35219997 packets.
   Snort ran for 0 days 0 hours 0 minutes 48 seconds
      Pkts/sec:       733749
   ...

   Performance counter stats for './snort -c snort.conf -r /pcaps/unsw-nb15/data1to10.pcap':

     211610.673317      task-clock (msec)         #    0.980 CPUs utilized          
            71,691      context-switches          #    0.339 K/sec                  
               120      cpu-migrations            #    0.001 K/sec                  
           261,595      page-faults               #    0.001 M/sec                  
   640,744,031,514      cycles                    #    3.028 GHz                    
   372,211,915,185      instructions              #    0.58  insns per cycle        
   103,539,551,596      branches                  #  489.293 M/sec                  
       471,878,057      branch-misses             #    0.46% of all branches        

     216.007758514 seconds time elapsed

Suricata
********

.. code:: bash

   alert http any any -> any any (content:"example.int"; http_host; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;)
   alert http any any -> any any (content:"lb.usemaxserver.de"; http_host; pcre:"/^.*(exe|bat|png).*$/"; msg:"Traffic"; sid:1; rev:1;)
   ...

With 9 processing packet threads

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/unsw-nb15/data1to10.pcap':

     149010.335565      task-clock (msec)         #    2.964 CPUs utilized          
         2,671,791      context-switches          #    0.018 M/sec                  
           189,310      cpu-migrations            #    0.001 M/sec                  
           106,145      page-faults               #    0.712 K/sec                  
   407,588,663,131      cycles                    #    2.735 GHz                    
   432,836,379,828      instructions              #    1.06  insns per cycle        
   103,898,788,650      branches                  #  697.259 M/sec                  
       586,897,592      branch-misses             #    0.56% of all branches        

      50.268667038 seconds time elapsed

Running suricata with one single thread

.. code:: bash

   Performance counter stats for './suricata --runmode single -c suricata.yaml -r /pcaps/unsw-nb15/data1to10.pcap':

     104787.575564      task-clock (msec)         #    1.778 CPUs utilized          
            81,378      context-switches          #    0.777 K/sec                  
               535      cpu-migrations            #    0.005 K/sec                  
           178,045      page-faults               #    0.002 M/sec                  
   305,054,917,165      cycles                    #    2.911 GHz                    
   454,603,357,798      instructions              #    1.49  insns per cycle        
   111,646,054,935      branches                  # 1065.451 M/sec                  
       487,762,061      branch-misses             #    0.44% of all branches        

      58.940150700 seconds time elapsed

AIEngine
********
TODO
.. code:: python

   rm = pyaiengine.RegexManager()
   r = pyaiengine.Regex("on the uri", "^.*(exe|png|bat).*$")
   rm.add_regex(r)

   h = pyaiengine.DomainName("domain_0", ".example.int")
   h.callback = http_callback
   h.http_uri_regex_manager = rm
   dm.add_domain_name(h)
   ....

.. code:: bash

   Performance counter stats for 'python demo5.py':

      26914.979850      task-clock (msec)         #    0.678 CPUs utilized          
           143,887      context-switches          #    0.005 M/sec                  
               210      cpu-migrations            #    0.008 K/sec                  
            12,668      page-faults               #    0.471 K/sec                  
    74,559,388,487      cycles                    #    2.770 GHz                    
    98,200,762,517      instructions              #    1.32  insns per cycle        
    19,013,557,409      branches                  #  706.430 M/sec                  
       150,866,117      branch-misses             #    0.79% of all branches        

      39.676431537 seconds time elapsed

+-------------+----------+--------------+---------+
| Test        | Cycles   | Instructions | Seconds |
+=============+==========+==============+=========+
| Snort       | 640.744M |     372.211M |     216 |
+-------------+----------+--------------+---------+
| Suricata(9) | 407.588M |     432.836M |      50 |
+-------------+----------+--------------+---------+
| Suricata(1) | 305.054M |     454.603M |      58 |
+-------------+----------+--------------+---------+
| AIEngine    |  74.559M |      98.200M |      39 |
+-------------+----------+--------------+---------+

Conclusions
...........

 - Not all the engines evaluated on these tests have the same functionality.
 - The traffic distribution have a big impact on the performance.
 - AIEngine shows a better performance in general with the given pcapsi also by calling python code.
